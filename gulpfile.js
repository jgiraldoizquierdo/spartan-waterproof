// Include gulp
var gulp = require('gulp');

// Include Our Plugins
var jshint = require('gulp-jshint');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var minifycss = require('gulp-minify-css');
var autoprefixer = require('gulp-autoprefixer');
var rename = require('gulp-rename');
var notify = require('gulp-notify');
var sourcemaps = require('gulp-sourcemaps');
var sassdoc = require('sassdoc');
var _ = require('lodash');
var clean = require('gulp-clean');
var imagemin = require('gulp-imagemin');

var sass_input = './app/css/sass/**/*.scss';
var sass_out = './app/css/';
var js_out = './app/js/';
var sassOptions = {
  errLogToConsole: false,//true
  sourceComments: true,//map
  outputStyle: 'nested'
};
var autoprefixerOptions = {
  browsers: ['last 2 versions', '> 5%', 'Firefox ESR','safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 3']
};
var sassdocOptions = {
  dest: './app/css/sassdoc'
};

// Lint Task
gulp.task('lint', function() {
    return gulp
        .src('app/js/main.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});

gulp.task('imagemin', function() {
    return gulp.src('app/images/**/**.*')
           .pipe(imagemin({
                progressive: true
           }))
           .pipe(gulp.dest('app/images/'))
});

// Compile Our Sass
gulp.task('sass', function() {
    return gulp
        .src(sass_input)
        .pipe(sass(sassOptions)).on('error', sass.logError)
        .pipe(sourcemaps.write())
        .pipe(autoprefixer(autoprefixerOptions))      
        /*.pipe(rename({
            suffix: '.min'
        }))*/
        //.pipe(minifycss())
        .pipe(gulp.dest(sass_out))
        
        .pipe(notify({
            message: "Generated file: <%= file.relative %>",
        }))   
        return gulp.src('app/tmp', {read: false})
        .pipe(clean()); 
});

gulp.task('sassdoc', function () {
  return gulp
    .src(sass_input)
    .pipe(sassdoc(sassdocOptions))
    .resume();
});

// Concatenate & Minify JS
gulp.task('scripts', function() {
    return gulp.src('app/js/module/**/*.js')
        //.pipe(sourcemaps.init())
        .pipe(concat('libs.js'))
        .pipe(gulp.dest(js_out))
        .pipe(rename("libs.min.js"))
        .pipe(uglify())
        //.pipe(sourcemaps.write("./"))
        .pipe(gulp.dest(js_out));
});

// Watch Files For Changes
gulp.task('watch', function() {
    gulp.watch('app/js/module/**/*.js', ['scripts']);
    gulp.watch(sass_input, ['sass'])
    .on('change', function(evt) {
        console.log(
            '[watcher] File ' + evt.path.replace(/.*(?=sass)/,'') + ' was ' + evt.type + ', compiling...'
        );
    });
});


// Default Task
gulp.task('default', ['apigen','imagemin','sassdoc', 'sass', 'scripts', 'watch']);
