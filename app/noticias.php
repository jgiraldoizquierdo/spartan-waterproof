<?php $page = isset($_GET['menu'])?$_GET['menu']:'noticias'; ?>

<?php include 'inc/configuracion.php'; ?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->

<?php include 'inc/head.php'; ?>

<body class="responsive">

    <!-- LOADING -->
    <div class="all_content loading">

        <?php include 'inc/header.php'; ?>
        
        <?php include 'inc/title.php'; ?>

        <!-- ALL CONTENTS -->
        <div class="dima-main">
        <!-- TEAM INFO -->
            <section class="section ">
                <div class="page-section-content overflow-hidden">
                    <div class="container">
                        <div class="text-center">
                            <div class="topaz-line">
                                <i class="di-separator"></i>
                            </div>
                            <div class="clear-section"></div>
                        </div>
                        <div class="ok-row">
                            <div class=" ok-md-6 ok-xsd-12 ok-sd-12">
                                <div class=" ok-md-6 ok-xsd-12">
                                    <div class="dima-team-member">
                                        <div class="team-img">
                                            <div class="fix-chrome">
                                                <figure>
                                                    <img src="images/news/news-01.jpg" alt="">
                                                </figure>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class=" ok-md-6 ok-xsd-12">
                                    <p><strong>1r Rally de regularidad de Brutosperodecicido</strong></p>
                                    <p>Bogotá - Colombia</p>
                                    <p>2 de noviembre de 2017</p>
                                    <p>Compartimos con ustedes nuestra grata experiencia como patrocinadores del 1r Rally de regularidad de Brutosperodecicidos. Una vez más probando que nada más resistente y guerrero que Spartan Waterproof.</p>
                                    <a class="btn-see-more" href="noticias-01.php">Ver más</a>
                                </div>
                            </div>
                            <div class=" ok-md-6 ok-xsd-12 ok-sd-12">
                                <div class=" ok-md-6 ok-xsd-12">
                                    <div class="dima-team-member">
                                        <div class="team-img">
                                            <div class="fix-chrome">
                                                <figure>
                                                    <img src="images/news/news-02.jpg" alt="">
                                                </figure>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class=" ok-md-6 ok-xsd-12">
                                    <p><strong>Encuentrenos en la fería de las dos ruedas en medellín.</strong></p>
                                    <p>Bogotá - Colombia</p>
                                    <p>3 de mayo de 2018</p>
                                    <p>A partir del 3 hasta el 6 de mayo encuentre nuestros productos en la feria de las dos ruedas en la ciudad de Medellín en la Plaza Mayor de Exposiciones en
 la Calle 41 No. 55-35 de 10:00 a.m. a 8:00 p.m., stand 547.</p>
                                    <a class="btn-see-more" href="noticias-02.php">Ver más</a>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </section>
            <!--! TEAM INFO -->
        </div>
        <!--! ALL CONTENTS -->

        <?php include 'inc/footer.php'; ?>

        <?php include 'inc/script.php'; ?>

    </div>
    <!--! LOADING -->

</body>

</html>
