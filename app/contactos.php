<?php $page = isset($_GET['menu'])?$_GET['menu']:'contactos'; ?>

<?php include 'inc/configuracion.php'; ?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->

<?php include 'inc/head.php'; ?>

<body class="responsive">

    <!-- LOADING -->
    <div class="all_content loading">

        <?php include 'inc/header.php'; ?>
        
        <?php include 'inc/title.php'; ?>

        <!-- ALL CONTENTS -->
        <div class="dima-main">
           <div class="page-section-content overflow-hidden">
            <div class="clear-section"></div>
            <div class="container">
               <div class="text-center">
                            <div class="topaz-line">
                                <i class="di-separator"></i>
                            </div>
                            <div class="clear-section"></div>
                        </div>
                <div class="dima-container float-start">
                    <p>Contactese con nosotros.</p>
                    <div class="clear"></div>
                    <div class="clear"></div>
                    <h5 class="uppercase">Contactenos</h5>
                    <div class="clear"></div>
                    <!-- Success Msg -->
                    <div id="contactSuccess" class="dima-alert dima-alert-success fade in hide">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <i class="fa  fa-check "></i>
                        <h4 class="header-alert">Success</h4>
                        <p>Your message has been sent to us.</p>
                    </div>
                    <!--! Success Msg -->
                    <!-- Error Msg -->
                    <div id="contactError" class="dima-alert dima-alert-error fade in hide">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <i class="fa fa-times "></i>
                        <h4 class="header-alert">Error!</h4>
                        <p> There was an error sending your message.</p>
                    </div>
                    <!--! Error Msg -->
                    <!-- Form -->
                    <form id="contact" action="inc/contact-form-smtp.php" class="form-small form text-center">
                        <div class="ok-row">
                            <div class="post ok-md-4 ok-xsd-6  ">
                                <div class="field">
                                    <input id="name" name="name" type="text" placeholder="Nombre" required>
                                </div>
                            </div>
                            <div class="post ok-md-4 ok-xsd-6  ">
                                <div class="field">
                                    <input id="email" name="email" type="email" placeholder="Email" required>
                                </div>
                            </div>
                            <div class="post ok-md-4 ok-xsd-6  ">
                                <div class="field">
                                    <input id="subject" name="subject" type="text" placeholder="Asunto" required>
                                </div>
                            </div>
                        </div>
                        <div class="field  ">
                            <textarea id="message" name="message" class="  textarea" placeholder="Mensaje" required></textarea>
                        </div>
                        <input type="submit" value="ENVIAR" class="no-rounded button small fill">
                    </form>
                    <!-- Form -->
                </div>
                <!-- SIDEBAR -->
                <aside role="complementary" class="dima-sidebar hidden-xsd float-end">
                    <!-- CONTACT INFO -->
                    <div class="widget">
                        <h4 class="widget-title uppercase">Información</h4>
                        <div class="widget-content">
                            <ul class="with-border featured-posts contact-icon">
                                <li>
                                    <i class="fa fa-map-marker "></i>
                                    <p>Bogotá - COLOMBIA</p>
                                </li>
                                <li>
                                    <i class="fa fa-phone"></i>
                                    <p>301 453 07 93</p>
                                </li>
                                <li>
                                    <i class="fa fa-envelope"></i>
                                    <p>informacion@sapartan.com</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!--! CONTACT INFO -->
                    <!-- SOCIAL NETWORK -->
                    <div class="widget">
                        <div class="widget-content">
                            <ul class="social-media social-big inline clearfix">
                                <li>
                                    <a data-animated-link="fadeOut" href="https://www.facebook.com/spartanwaterproof/" target="_blank">
                                        <i class="fa fa-facebook"></i>
                                    </a>
                                </li>
                                <!--li>
                                    <a data-animated-link="fadeOut" href="#">
                                        <i class="fa fa-twitter"></i>
                                    </a>
                                </li>
                                <li>
                                    <a data-animated-link="fadeOut" href="#">
                                        <i class="fa fa-google-plus"></i>
                                    </a>
                                </li-->
                                <li>
                                    <a data-animated-link="fadeOut" href="https://www.instagram.com/spartanwaterproof/">
                                        <i class="fa fa-instagram"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!--! SOCIAL NETWORK -->
                </aside>
                <!--! SIDEBAR -->
            </div>
            </div>
            <div class="double-clear"></div>
        </div>
        <!--! ALL CONTENTS -->

        <?php include 'inc/footer.php'; ?>

        <?php include 'inc/script.php'; ?>

    </div>
    <!--! LOADING -->

</body>

</html>
