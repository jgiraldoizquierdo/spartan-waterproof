<?php $page = isset($_GET['menu'])?$_GET['menu']:'compania'; ?>

<?php include 'inc/configuracion.php'; ?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->

<?php include 'inc/head.php'; ?>

<body class="responsive">

    <!-- LOADING -->
    <div class="all_content loading">

        <?php include 'inc/header.php'; ?>
        
        <?php include 'inc/title.php'; ?>

        <!-- ALL CONTENTS -->
        <div class="dima-main">
            
            <!-- ABOUT SECTION -->
            <section class="section section-colored" data-bg="#fafafa" id="about">
                <div class="page-section-content overflow-hidden">
                    <div class="container text-center">
                        <div class="ok-row">
                            <!-- TITLE -->
                            <h2 class="uppercase" data-animate="fadeInDown" data-delay="0">Acerca de nosotros</h2>
                            <div class="topaz-line">
                                <i class="di-separator"></i>
                            </div>
                            <p data-animate="fadeInUp" data-delay="100">Somos una compañía joven nacida entre Bogotá y Medellín, fabricamos accesorios para las motos y los motociclistas Colombianos, mejoramos la experiencia de viajar en una motocicleta, creando productos prácticos, innovadores y de excelente calidad para la tranquilidad y confort de nuestros clientes.</p>
                            <!--! TITLE -->
                            <div class="double-clear"></div>

                            <div class="ok-md-6 ok-xsd-12 text-start">
                                <h4 class="uppercase" data-animate="fadeInUp" data-delay="200">Misión</h4>
                                <p data-animate="fadeInUp" data-delay="220">Crear productos de alta calidad, resistentes al agua y polvo elementos presentes en las vias y el día a día de los motocilcistas, pensados para adaptarse a las condiciones de las distintas motos y sus requerimientos.
                                    <a data-animated-link="fadeOut" href="#">Mejorar</a> los diseños y satisfacer condiciones del mercado a la solicitud de nuestros clientes en el país y fuere de el.</p>
                                <div class="double-clear"></div>
                            </div>
                            <!-- IMAGEN -->
                            <div class="ok-md-6 ok-xsd-12">
                               <img src="images/sections/compania-01.jpg" alt="">
                            </div>
                            <!--! IMAGEN -->
                        </div>
                        <div class="double-clear"></div>
                        <div class="ok-row">
                           <!-- IMAGEN -->
                            <div class="ok-md-6 ok-xsd-12">
                               <img src="images/sections/compania-02.jpg" alt="">
                            </div>
                            <!--! IMAGEN -->
                            <div class="ok-md-6 ok-xsd-12 text-start">
                                <h4 class="uppercase" data-animate="fadeInUp" data-delay="200">Visión</h4>
                                <p data-animate="fadeInUp" data-delay="220">nuestra labor se basa en entregar productos de excelente calidad que tengan un largo tiempo de vida y uso, es nuestra labor mejorarlos y ofrecer la más alta calidad.
</p>
                                <div class="double-clear"></div>
                            </div>
                        </div>
                        <div class="double-clear"></div>
                        <div class="ok-row">

                            <div class="ok-md-6 ok-xsd-12 text-start">
                                <h4 class="uppercase" data-animate="fadeInUp" data-delay="200">OBJETIVOS</h4>
                                <p data-animate="fadeInUp" data-delay="220">En los próximos cinco años trabajaremos en convertirnos en la primera empresa de fabricación en Colombia,
                                    <a data-animated-link="fadeOut" href="#">Nos proyectaremos</a> en mercados internacionales en la busqueda de convertirnos en una empresa reconocida a nivel mundial en fabricación de drybags de alta calidad.</p>
                                <div class="double-clear"></div>
                            </div>
                            <!-- IMAGEN -->
                            <div class="ok-md-6 ok-xsd-12">
                               <img src="images/sections/compania-03.jpg" alt="">
                            </div>
                            <!--! IMAGEN -->
                        </div>
                    </div>
                </div>
            </section>
            <!--! ABOUT SECTION -->
            
            
            <!--! CLIENTS SECTION -->
            <div class="container text-center">
                <div class="double-clear"></div>
                <div class="clients-wrapper">
                   <!-- TITLE -->
                        <div class="topaz-line">
                            <i class="di-separator"></i>
                        </div>
                        <h2 data-animate="fadeInDown" data-delay="0">Productos</h2>
                        <!--! TITLE -->
                    
                    <div class="boxed-blog owl-carousel owl-darck" data-owl-namber="6" data-owl-phone="1" data-owl-tablet="4" data-owl-margin="3"
                        data-owl-autoPlay="true" data-owl-loop="true" data-owl-navigation="false" data-owl-pagination="false">
                         <div class="client">
                            <a data-animated-link="fadeOut" href="#">
                                <img src="http://fpoimg.com/187x132" alt="">
                            </a>
                        </div>
                        <div class="client">
                            <a data-animated-link="fadeOut" href="#">
                                <img src="http://fpoimg.com/187x132" alt="">
                            </a>
                        </div>
                        <div class="client">
                            <a data-animated-link="fadeOut" href="#">
                                <img src="http://fpoimg.com/187x132" alt="">
                            </a>
                        </div>
                        <div class="client">
                            <a data-animated-link="fadeOut" href="#">
                                <img src="http://fpoimg.com/187x132" alt="">
                            </a>
                        </div>
                        <div class="client">
                            <a data-animated-link="fadeOut" href="#">
                                <img src="http://fpoimg.com/187x132" alt="">
                            </a>
                        </div>
                        <div class="client">
                            <a data-animated-link="fadeOut" href="#">
                                <img src="http://fpoimg.com/187x132" alt="">
                            </a>
                        </div>
                        <div class="client">
                            <a data-animated-link="fadeOut" href="#">
                                <img src="http://fpoimg.com/187x132" alt="">
                            </a>
                        </div>
                        <div class="client">
                            <a data-animated-link="fadeOut" href="#">
                                <img src="http://fpoimg.com/187x132" alt="">
                            </a>
                        </div>
                        <div class="client">
                            <a data-animated-link="fadeOut" href="#">
                                <img src="http://fpoimg.com/187x132" alt="">
                            </a>
                        </div>
                        <div class="client">
                            <a data-animated-link="fadeOut" href="#">
                                <img src="http://fpoimg.com/187x132" alt="">
                            </a>
                        </div>
                    </div>
                    <div class="prev-next">
                        <a class="prev"></a>
                        <a class="next"></a>
                    </div>
                </div>
                <div class="double-clear"></div>
            </div>
            <!--! CLIENTS SECTION -->
        </div>
        <!--! ALL CONTENTS -->

        <?php include 'inc/footer.php'; ?>

        <?php include 'inc/script.php'; ?>

    </div>
    <!--! LOADING -->

</body>

</html>
