<?php $page = isset($_GET['menu'])?$_GET['menu']:'productos'; ?>

<?php include 'inc/configuracion.php'; ?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->

<?php include 'inc/head.php'; ?>

<body class="responsive">

    <!-- LOADING -->
    <div class="all_content loading">

        <?php include 'inc/header.php'; ?>
        
        <?php include 'inc/title.php'; ?>

        <!-- ALL CONTENTS -->
        <div class="dima-main">
          <section class="section section-colored" data-bg="#fafafa" style="background-color: rgb(250, 250, 250);">
                    <div class="page-section-content overflow-hidden">
                        <div class="container ">
                            
                            <div class="ok-row">
                                <!-- PRODUCT (1) -->
                                <div class=" ok-md-12 ok-xsd-12 ok-sd-12 border no-padding">
                                    
                                    <div class=" ok-md-4 ok-xsd-12">
                                        <h5>BAG LUGGAGE</h5>
                                        <h6 class="theme-color">Waterproof</h6>
                                        <p>Puedes combinar 2 colores de Lona y  uno de reata,  también puedes elegir el sistema de anclaje (incorporado), también la personalizamos con tu nombre y así tendrás un producto que nadie más tiene un producto tan único como tu. 
</p>
                                        <div class="clear"></div>
                                        <strong>Litrajes disponibles:</strong>
                                        <div class="clear"></div>
                                        <h3 class="theme-color">23LTS</h3>
                                    </div>
                                    <div class=" ok-md-8 ok-xsd-12 no-padding">
                                        <div class="dima-team-member">
                                            <div class="team-img">
                                                <div class="fix-chrome">
                                                   <!--div class="valuebag"><p>23l</p></div-->
                                                   <a href="bagluggage.php" class="btn-seemore"><p>Ver más</p></a>
                                                    <figure>
                                                        <img src="images/productos/foto-01.jpg" alt="">
                                                    </figure>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                                <!-- PRODUCT (1) -->
                                <div class="clear-section"></div>
                                    <div class="topaz-line">
                                        <i class="di-separator"></i>
                                    </div>
                               <div class="clear-section"></div>
                                <!-- PRODUCT (2) -->
                                <div class=" ok-md-12 ok-xsd-12 ok-sd-12 border no-padding">
                                    <div class=" ok-md-4 ok-xsd-12">
                                        <h5>DRYBAG</h5>
                                        <h6 class="theme-color">Waterproof</h6>
                                        <p>Puedes combinar 2 colores de Lona y  uno de reata,  también puedes elegir el sistema de anclaje (incorporado), también la personalizamos con tu nombre y así tendrás un producto que nadie más tiene un producto tan único como tu.</p>
                                        <div class="clear"></div>
                                        <strong>Litrajes disponibles</strong>
                                        <div class="clear"></div>
                                        <h3 class="theme-color">23LTS , 39LTS, 60LTS</h3>
                                    </div>
                                    <div class=" ok-md-8 ok-xsd-12 no-padding">
                                        <div class="dima-team-member">
                                            <div class="team-img">
                                                <div class="fix-chrome">
                                                   <!--div class="valuebag"><p>60l</p></div-->
                                                   <a href="drybag.php" class="btn-seemore"><p>Ver más</p></a>
                                                    <figure>
                                                        <img src="images/productos/foto-02.jpg" alt="">
                                                    </figure>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- PRODUCT (2) -->
                                <div class="clear-section"></div>
                                    <div class="topaz-line">
                                        <i class="di-separator"></i>
                                    </div>
                               <div class="clear-section"></div>
                               <!-- PRODUCT (3) -->
                                <div class=" ok-md-12 ok-xsd-12 ok-sd-12 border no-padding">
                                    <div class=" ok-md-4 ok-xsd-12">
                                        <h5>ROLL BAG</h5>
                                        <h6 class="theme-color">Waterproof</h6>
                                        <p>Puedes combinar color de Lona con  el  reata, también la personalizamos con tu nombre y así tendrás un producto que nadie más tiene un producto tan único como tú.</p>
                                        <div class="clear"></div>
                                        <strong>Litrajes disponibles</strong>
                                        <div class="clear"></div>
                                        <h3 class="theme-color">12LTS</h3>
                                    </div>
                                    <div class=" ok-md-8 ok-xsd-12 no-padding">
                                        <div class="dima-team-member">
                                            <div class="team-img">
                                                <div class="fix-chrome">
                                                   <!--div class="valuebag"><p>60l</p></div-->
                                                   <a href="rollbag.php" class="btn-seemore"><p>Ver más</p></a>
                                                    <figure>
                                                        <img src="images/productos/foto-03.jpg" alt="">
                                                    </figure>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- PRODUCT (3) -->
                                <div class="clear-section"></div>
                                    <div class="topaz-line">
                                        <i class="di-separator"></i>
                                    </div>
                               <div class="clear-section"></div>
                               <!-- PRODUCT (3) -->
                                <div class=" ok-md-12 ok-xsd-12 ok-sd-12 border no-padding">
                                    <div class=" ok-md-4 ok-xsd-12">
                                        <h5>TRUNK BAG</h5>
                                        <h6 class="theme-color">Waterproof</h6>
                                        <p>Puedes combinar 2 colores de Lona y  uno de reata,  también puedes elegir el sistema de anclaje (incorporado), también la personalizamos con tu nombre y así tendrás un producto que nadie más tiene un producto tan único como tu.</p>
                                        <div class="clear"></div>
                                        <strong>Litrajes disponibles</strong>
                                        <div class="clear"></div>
                                        <h3 class="theme-color">23LTS</h3>
                                    </div>
                                    <div class=" ok-md-8 ok-xsd-12 no-padding">
                                        <div class="dima-team-member">
                                            <div class="team-img">
                                                <div class="fix-chrome">
                                                   <!--div class="valuebag"><p>60l</p></div-->
                                                   <a href="trunkbag.php" class="btn-seemore"><p>Ver más</p></a>
                                                    <figure>
                                                    <img src="images/productos/foto-04.jpg" alt="">
                                                    </figure>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- PRODUCT (3) -->
                            </div>
                        </div>
                    </div>
                </section>
        </div>
        <!--! ALL CONTENTS -->

        <?php include 'inc/footer.php'; ?>

        <?php include 'inc/script.php'; ?>

    </div>
    <!--! LOADING -->

</body>

</html>
