<?php $page = isset($_GET['menu'])?$_GET['menu']:'productos'; ?>

<?php include 'inc/configuracion.php'; ?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->

<?php include 'inc/head.php'; ?>

<body class="responsive">

    <!-- LOADING -->
    <div class="all_content loading">

        <?php include 'inc/header.php'; ?>
        
        <?php include 'inc/title.php'; ?>

        <!-- ALL CONTENTS -->
        <div class="dima-main">
          <section class="section section-colored" data-bg="#fafafa" style="background-color: rgb(250, 250, 250);">
            <div class="page-section-content overflow-hidden">
                    <div class="container">
                        <!-- WORKS -->
                        <div class="dima-container float-start">
                            <div class="post">
                                <div class="post-img">
                                    <div class="flexslider slider-thumb small-control" data-animation="slide" data-loop="true" data-nav="thumbnails">
                                        <ul class="slides">
                                            <!-- WORK (1) -->
                                            <li class="slide-item" data-thumb="images/productos/trunkbag/small/TRUNK-01.jpg">
                                                <div class="flex-img">
                                                    <img src="images/productos/trunkbag/big/TRUNK-01.jpg">
                                                </div>
                                            </li>
                                            <!-- WORK (1) -->
                                            <!-- WORK (2) -->
                                            <li class="slide-item" data-thumb="images/productos/trunkbag/small/TRUNK-02.jpg">
                                                <div class="flex-img">
                                                    <img src="images/productos/trunkbag/big/TRUNK-02.jpg">
                                                </div>
                                            </li>
                                            <!-- WORK (2) -->
                                            <!-- WORK (3) -->
                                            <li class="slide-item" data-thumb="images/productos/trunkbag/small/TRUNK-03.jpg">
                                                <div class="flex-img">
                                                    <img src="images/productos/trunkbag/big/TRUNK-03.jpg">
                                                </div>
                                            </li>
                                            <!-- WORK (3) -->
                                            <!-- WORK (4) -->
                                            <li class="slide-item" data-thumb="images/productos/trunkbag/small/TRUNK-04.jpg">
                                                <div class="flex-img">
                                                    <img src="images/productos/trunkbag/big/TRUNK-04.jpg">
                                                </div>
                                            </li>
                                            <!-- WORK (4) -->
                                            <!-- WORK (5) -->
                                            <li class="slide-item" data-thumb="images/productos/trunkbag/small/TRUNK-05.jpg">
                                                <div class="flex-img">
                                                    <img src="images/productos/trunkbag/big/TRUNK-05.jpg">
                                                </div>
                                            </li>
                                            <!-- WORK (5) -->
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--! WORKS -->
                        <aside role="complementary" class="dima-sidebar float-end hidden-xsd">
                            <!-- PROJECT INFO -->
                            <div class="box box-with-marge">
                                <h5 class="uppercase">TRUNK BAG</h5>
                                <div class="clear"></div>
                                <div class="box-content">
                                    <p>Puedes combinar color de Lona con  el  reata, también la personalizamos con tu nombre y así tendrás un producto que nadie más tiene un producto tan único como tú.</p>
                                </div>
                            </div>
                            <!--! PROJECT INFO -->
                            <!-- PROJECT DETAILS -->
                            <div class="box box-with-marge">
                                <h4 class="uppercase box-titel">ficha técnica</h4>
                                <div class="box-content">
                                    <ul class="with-border">
                                    <li>
                                        Hebillas contramarcadas
                                        </li>
                                        <li>
                                        2 argollas 
                                        </li>
                                        <li>
100% impermeable, 100% a prueba de polvo y olores.	
</li>
<li>
100% Producto Colombiano.
</li>
                                        <li>
                                            <strong class="uppercase">Capacidad</strong>: 25lts</li>
                                        <li>
                                            <strong class="uppercase">Ancho</strong>: 43cm</li>
                                            <li>
                                            <strong class="uppercase">Alto</strong>: 32cm</li>
                                            <li>
                                            <strong class="uppercase">Largo</strong>: 19cm</li>
                                        <li>
                                            <strong class="uppercase">Impermeable</strong>: si</li>
                                        <li>
                                            <h4 class="theme-color">$230.000  El par.</h4>
                                        </li>
                                        <li>
                                            <strong class="uppercase">Capacidad</strong>: 45lts</li>
                                        <li>
                                            <strong class="uppercase">Ancho</strong>: 43cm</li>
                                            <li>
                                            <strong class="uppercase">Alto</strong>: 32cm</li>
                                            <li>
                                            <strong class="uppercase">Largo</strong>: 27cm</li>
                                        <li>
                                            <strong class="uppercase">Impermeable</strong>: si</li>
                                        <li>
                                            <h4 class="theme-color">$230.000  El par.</h4>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <!--! PROJECT DETAILS -->
                        </aside>
                    </div>
                </div>        
          </section>
        </div>
        <!--! ALL CONTENTS -->

        <?php include 'inc/footer.php'; ?>

        <?php include 'inc/script.php'; ?>

    </div>
    <!--! LOADING -->

</body>

</html>
