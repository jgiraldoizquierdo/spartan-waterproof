
            <!--! NEWS SECTION -->
            <div class="container text-center">
                <div class="double-clear"></div>
                <div class="clients-wrapper">
                   <!-- TITLE -->
                        <div class="topaz-line">
                            <i class="di-separator"></i>
                        </div>
                        <h2 data-animate="fadeInDown" data-delay="0">Noticias</h2>
                        <p data-animate="fadeInUp" data-delay="0">Novedades acerca del universo de dos ruedas en Colombia y el mundo.
                            <br>Acompañadas de notas sobre nuestros productos y sus compradores.</p>
                        <!--! TITLE -->
                        
                    
                        <div class="double-clear"></div>
                        <div class="boxed-blog">
                            <!-- POST (1) -->
                            <article role="article" class="ok-md-6 ok-xsd-6" data-animate="fadeInUp" data-delay="50">
                                <div class="post e-post">
                                    <div class="post-img">
                                        <img src="images/news/news-01.jpg" alt="Noticia 01">
                                    </div>
                                    
                                    <div class="post-content text-start box">
                                        <p>
                                            <a data-animated-link="fadeOut" href="#"><strong>1r Rally de regularidad de Brutosperodecicido</strong></a>
                                            <span>Bogotá - Colombia</span>
                                        </p>
                                        <div class="post-meta box">
                                        <ul>
                                            <li class="post-on">2 de noviembre de 2017</li>
                                        </ul>
                                    </div>
                                        <p class="flat-paragraph">Compartimos con ustedes nuestra grata experiencia como patrocinadores del 1r Rally de regularidad de Brutosperodecicidos. Una vez más probando que nada más resistente y guerrero que Spartan Waterproof.</p>
                                        <a class="btn-see-more" href="noticias-01.php">Ver más</a>
                                    </div>
                                </div>
                            </article>
                            <!--! POST (1) -->
                            <!-- POST (2) -->
                            <article role="article" class="ok-md-6 ok-xsd-6" data-animate="fadeInUp" data-delay="150">
                                <div class="post e-post">
                                    <div class="post-img ">
                                        <img src="images/news/news-02.jpg" alt="Noticia 01">
                                    </div>
                                    
                                    <div class="post-content text-start box">
                                        <p>
                                            <a data-animated-link="fadeOut" href="#"><strong>Encuentrenos en la fería de las dos ruedas en medellín.</strong></a>
                                            <span>Medellín - Colombia</span>
                                        </p>
                                        <div class="post-meta box">
                                        <ul>
                                            <li class="post-on">3 de mayo de 2018</li>
                                        </ul>
                                    </div>
                                        <p class="flat-paragraph">A partir del 3 hasta el 6 de mayo encuentre nuestros productos en la feria de las dos ruedas en la ciudad de Medellín en la Plaza Mayor de Exposiciones en
 la Calle 41 No. 55-35 de 10:00 a.m. a 8:00 p.m., stand 547.</p>
                                        <a class="btn-see-more" href="noticias-02.php">Ver más</a>
                                    </div>
                                </div>
                            </article>
                            <!--! POST (2) -->
                        </div>
                    
                </div>
                <div class="double-clear"></div>
            </div>
            <!--! NEWS SECTION -->
