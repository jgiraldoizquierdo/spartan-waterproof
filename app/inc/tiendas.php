<!--! CLIENTS SECTION -->
         <div class="container text-center">
                <div class="double-clear"></div>
                <div class="clients-wrapper">
                   <!-- TITLE -->
                        <div class="topaz-line">
                            <i class="di-separator"></i>
                        </div>
                        <h2 data-animate="fadeInDown" data-delay="0">Tiendas</h2>
                        <p data-animate="fadeInUp" data-delay="0">Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis
                            <br>bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit.</p>
                        <!--! TITLE -->
                    <div class="double-clear"></div>
                    <div class="boxed-blog owl-carousel owl-darck" data-owl-namber="6" data-owl-phone="1" data-owl-tablet="4" data-owl-margin="3"
                        data-owl-autoPlay="true" data-owl-loop="true" data-owl-navigation="false" data-owl-pagination="false">
                        <div class="client">
                            <a data-animated-link="fadeOut" href="#">
                                <img src="http://fpoimg.com/187x132" alt="">
                            </a>
                        </div>
                        <div class="client">
                            <a data-animated-link="fadeOut" href="#">
                                <img src="http://fpoimg.com/187x132" alt="">
                            </a>
                        </div>
                        <div class="client">
                            <a data-animated-link="fadeOut" href="#">
                                <img src="http://fpoimg.com/187x132" alt="">
                            </a>
                        </div>
                        <div class="client">
                            <a data-animated-link="fadeOut" href="#">
                                <img src="http://fpoimg.com/187x132" alt="">
                            </a>
                        </div>
                        <div class="client">
                            <a data-animated-link="fadeOut" href="#">
                                <img src="http://fpoimg.com/187x132" alt="">
                            </a>
                        </div>
                        <div class="client">
                            <a data-animated-link="fadeOut" href="#">
                                <img src="http://fpoimg.com/187x132" alt="">
                            </a>
                        </div>
                        <div class="client">
                            <a data-animated-link="fadeOut" href="#">
                                <img src="http://fpoimg.com/187x132" alt="">
                            </a>
                        </div>
                        <div class="client">
                            <a data-animated-link="fadeOut" href="#">
                                <img src="http://fpoimg.com/187x132" alt="">
                            </a>
                        </div>
                        <div class="client">
                            <a data-animated-link="fadeOut" href="#">
                                <img src="http://fpoimg.com/187x132" alt="">
                            </a>
                        </div>
                        <div class="client">
                            <a data-animated-link="fadeOut" href="#">
                                <img src="http://fpoimg.com/187x132" alt="">
                            </a>
                        </div>
                    </div>
                    <div class="prev-next">
                        <a class="prev"></a>
                        <a class="next"></a>
                    </div>
                </div>
                <div class="double-clear"></div>
            </div>
            <!--! CLIENTS SECTION -->