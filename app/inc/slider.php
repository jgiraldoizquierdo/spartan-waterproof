<!-- SLIDER -->
        <div class="tp-banner-container">
            <div class="tp-banner fullscreen">
                <ul>
                    <!-- SLIDE  -->
                    <li data-transition="fade" data-slotamount="1" data-masterspeed="1500" data-delay="13000" data-saveperformance="off" data-title="Our Workplace">
                        <!-- MAIN IMAGE -->
                        <img src="images/slides/slide-01.jpg" alt="kenburns1" data-bgposition="left center" data-kenburns="on" data-duration="14000"
                            data-ease="Linear.easeNone" data-bgfit="100" data-bgfitend="130" data-bgpositionend="right center">
                        <!-- LAYERS -->
                        <h1 class="undertitle tp-caption skewfromleft customout rs-parallaxlevel-0" data-x="center" data-y="center" data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                            data-speed="530" data-start="700" data-easing="Power3.easeInOut" data-elementdelay="0.1" data-endelementdelay="0.1">
                            <span class="theme-color">Spartan </span>Waterproof</h1>
                        <div class="flex-caption undertitle tp-caption skewfromleft customout rs-parallaxlevel-0" data-x="center" data-y="320" data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                            data-speed="530" data-start="900" data-easing="Power3.easeInOut" data-elementdelay="0.1" data-endelementdelay="0.1">
                        </div>
                        <div class="pattern-slider"></div>
                    </li>
                    <li data-transition="fade" data-slotamount="1" data-masterspeed="1500" data-delay="13000" data-saveperformance="off" data-title="Our Workplace">
                        <!-- MAIN IMAGE -->
                        <img src="images/slides/slide-02.jpg" alt="kenburns1" data-bgposition="left center" data-kenburns="on" data-duration="14000"
                            data-ease="Linear.easeNone" data-bgfit="100" data-bgfitend="130" data-bgpositionend="right center">
                        <!-- LAYERS -->
                        <h1 class="undertitle tp-caption skewfromleft customout rs-parallaxlevel-0" data-x="center" data-y="center" data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                            data-speed="530" data-start="700" data-easing="Power3.easeInOut" data-elementdelay="0.1" data-endelementdelay="0.1">
                            <span class="theme-color">Resistente</h1>
                        <div class="flex-caption undertitle tp-caption skewfromleft customout rs-parallaxlevel-0" data-x="center" data-y="320" data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                            data-speed="530" data-start="900" data-easing="Power3.easeInOut" data-elementdelay="0.1" data-endelementdelay="0.1">
                        </div>
                        <div class="pattern-slider"></div>
                    </li>
                    <li data-transition="fade" data-slotamount="1" data-masterspeed="1500" data-delay="13000" data-saveperformance="off" data-title="Our Workplace">
                        <!-- MAIN IMAGE -->
                        <img src="images/slides/slide-03.jpg" alt="kenburns1" data-bgposition="left center" data-kenburns="on" data-duration="14000"
                            data-ease="Linear.easeNone" data-bgfit="100" data-bgfitend="130" data-bgpositionend="right center">
                        <!-- LAYERS -->
                        <h1 class="undertitle tp-caption skewfromleft customout rs-parallaxlevel-0" data-x="center" data-y="center" data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                            data-speed="530" data-start="700" data-easing="Power3.easeInOut" data-elementdelay="0.1" data-endelementdelay="0.1">
                            <span class="theme-color">Impermeable</h1>
                        <div class="flex-caption undertitle tp-caption skewfromleft customout rs-parallaxlevel-0" data-x="center" data-y="320" data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                            data-speed="530" data-start="900" data-easing="Power3.easeInOut" data-elementdelay="0.1" data-endelementdelay="0.1">
                        </div>
                        <div class="pattern-slider"></div>
                    </li>
                    <li data-transition="fade" data-slotamount="1" data-masterspeed="1500" data-delay="13000" data-saveperformance="off" data-title="Our Workplace">
                        <!-- MAIN IMAGE -->
                        <img src="images/slides/slide-04.jpg" alt="kenburns1" data-bgposition="left center" data-kenburns="on" data-duration="14000"
                            data-ease="Linear.easeNone" data-bgfit="100" data-bgfitend="130" data-bgpositionend="right center">
                        <!-- LAYERS -->
                        <h1 class="undertitle tp-caption skewfromleft customout rs-parallaxlevel-0" data-x="center" data-y="center" data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                            data-speed="530" data-start="700" data-easing="Power3.easeInOut" data-elementdelay="0.1" data-endelementdelay="0.1">
                            <span class="theme-color">Versatil</span></h1>
                        <div class="flex-caption undertitle tp-caption skewfromleft customout rs-parallaxlevel-0" data-x="center" data-y="320" data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                            data-speed="530" data-start="900" data-easing="Power3.easeInOut" data-elementdelay="0.1" data-endelementdelay="0.1">
                        </div>
                        <div class="pattern-slider"></div>
                    </li>
                    <li data-transition="fade" data-slotamount="1" data-masterspeed="1500" data-delay="13000" data-saveperformance="off" data-title="Our Workplace">
                        <!-- MAIN IMAGE -->
                        <img src="images/slides/slide-05.jpg" alt="kenburns1" data-bgposition="left center" data-kenburns="on" data-duration="14000"
                            data-ease="Linear.easeNone" data-bgfit="100" data-bgfitend="130" data-bgpositionend="right center">
                        <!-- LAYERS -->
                        <h1 class="undertitle tp-caption skewfromleft customout rs-parallaxlevel-0" data-x="center" data-y="center" data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                            data-speed="530" data-start="700" data-easing="Power3.easeInOut" data-elementdelay="0.1" data-endelementdelay="0.1">
                            <span class="theme-color">Adaptable</h1>
                        <div class="flex-caption undertitle tp-caption skewfromleft customout rs-parallaxlevel-0" data-x="center" data-y="320" data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                            data-speed="530" data-start="900" data-easing="Power3.easeInOut" data-elementdelay="0.1" data-endelementdelay="0.1">
                        </div>
                        <div class="pattern-slider"></div>
                    </li>
                    <li data-transition="fade" data-slotamount="1" data-masterspeed="1500" data-delay="13000" data-saveperformance="off" data-title="Our Workplace">
                        <!-- MAIN IMAGE -->
                        <img src="images/slides/slide-06.jpg" alt="kenburns1" data-bgposition="left center" data-kenburns="on" data-duration="14000"
                            data-ease="Linear.easeNone" data-bgfit="100" data-bgfitend="130" data-bgpositionend="right center">
                        <!-- LAYERS -->
                        <h1 class="undertitle tp-caption skewfromleft customout rs-parallaxlevel-0" data-x="center" data-y="center" data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                            data-speed="530" data-start="700" data-easing="Power3.easeInOut" data-elementdelay="0.1" data-endelementdelay="0.1">
                            <span class="theme-color">100% </span>Colombiana</h1>
                        <div class="flex-caption undertitle tp-caption skewfromleft customout rs-parallaxlevel-0" data-x="center" data-y="320" data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                            data-speed="530" data-start="900" data-easing="Power3.easeInOut" data-elementdelay="0.1" data-endelementdelay="0.1">
                            <p class="undertitle hide-on-m text-center">Spartan Waterproof. Es una iniciativa orgullosamente Colombiana
                                <br>Para los moteros colombianos.</p>
                        </div>
                        <div class="pattern-slider"></div>
                    </li>
                    <!--li data-transition="slidehorizontal" data-slotamount="1" data-masterspeed="1500" data-thumb="images/slides/firstframe.jpg"
                        data-fstransition="fade" data-fsmasterspeed="1000" data-fsslotamount="7" data-saveperformance="off" data-title="Free on the Beach">
                        <!-- MAIN IMAGE -->
                        <!--img src="images/slides/slide-02.jpg" alt="video_woman_cover3" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat">
                        <!-- LAYERS -->
                        <!--div class="pattern-slider"></div>
                        <!-- LAYER NR. 1 -->
                        <!--div class="tp-caption tp-fade fadeout fullscreenvideo" data-x="0" data-y="0" data-speed="1000" data-start="1100" data-easing="Power4.easeOut"
                            data-elementdelay="0.01" data-endelementdelay="0.1" data-endspeed="1500" data-endeasing="Power4.easeIn"
                            data-autoplay="true" data-autoplayonlyfirsttime="false" data-nextslideatend="true" data-volume="mute"
                            data-forceCover="1" data-aspectratio="16:9" data-forcerewind="on" style="z-index: 2;">
                            <video preload="none" width="100%" height="100%" poster='images/slides/FirstFrame.jpg'>
                                <source src="images/slides/turbine.mp4" type='video/mp4' />
                                <source src='images/slides/turbine.webm' type='video/webm' />
                            </video>
                        </div>
                        <h1 class="tp-caption undertitle text-center skewfromleft customout rs-parallaxlevel-0" data-x="center" data-y="center" data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                            data-speed="530" data-start="700" data-easing="Power3.easeInOut" data-elementdelay="0.1" data-endelementdelay="0.1">
                            <span class="theme-color">Spartan </span>waterproof</h1>
                        <div class="flex-caption undertitle tp-caption skewfromleft customout rs-parallaxlevel-0" data-x="center" data-y="320" data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                            data-speed="530" data-start="900" data-easing="Power3.easeInOut" data-elementdelay="0.1" data-endelementdelay="0.1">
                            <p class="undertitle hide-on-m text-center">Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem
                                <br>quis bibendum auctor.</p>
                        </div>
                    </li-->
                </ul>
            </div>
        </div>
        <!--! SLIDER -->