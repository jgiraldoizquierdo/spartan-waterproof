<!-- SCREENSHOTS SECTION -->
            <section class="section " id="screenshots">
                <div class="page-section-content overflow-hidden">
                    <div class="container text-center">
                        <!-- TITLE -->
                        <div class="topaz-line">
                            <i class="di-separator"></i>
                        </div>
                        <h2 data-animate="fadeInDown" data-delay="0">Spartans</h2>
                        <p data-animate="fadeInUp" data-delay="0">Nuestros clientes envían sus fotos y comparten sus experiencias en compañía de nuestros productos<br>quien mejor que ellos para avalar la calidad de sus maletas Spartan Waterproof.</p>
                        <!--! TITLE -->
                        <div class="clear-section"></div>
                        <div class="double-clear"></div>
                        <div id="owl" class="owl-carousel" data-owl-namber="5" data-owl-tablet="3" data-owl-margin="15" data-owl-autoPlay="true"
                        data-owl-mousewheel="true" data-owl-loop="false" data-owl-navigation="false" data-owl-pagination="false">
                        <!-- PHOTO (1) -->
                        <div class="isotope-item">
                            <div class="work-item">
                                <div class="dima-masonry-100"></div>
                                <img src="images/spartans/spartan-01.jpg" alt="Brand Consulting">
                                <div class="link_overlay">
                                     <div class="testimonial">
                                        <div>
                                           <p><strong>Spartans</strong></p>
                                           <p></p>
                                       
                                            <p><a>Conoce más</a></p>
                                        </div>
                                    </div>
                                    <span class="topaz-hover"></span>
                                </div>
                            </div>
                        </div>
                        <!--! PHOTO (1) -->
                        <!-- PHOTO (1) -->
                        <div class="isotope-item">
                            <div class="work-item">
                                <div class="dima-masonry-100"></div>
                                <img src="images/spartans/spartan-02.jpg" alt="Brand Consulting">
                                <div class="link_overlay">
                                     <div class="testimonial">
                                        <div>
                                           <p><strong>Spartans</strong></p>
                                           <p></p>
                                       
                                            <p><a>Conoce más</a></p>
                                        </div>
                                    </div>
                                    <span class="topaz-hover"></span>
                                </div>
                            </div>
                        </div>
                        <!--! PHOTO (1) -->
                        <!-- PHOTO (1) -->
                        <div class="isotope-item">
                            <div class="work-item">
                                <div class="dima-masonry-100"></div>
                                <img src="images/spartans/spartan-03.jpg" alt="Brand Consulting">
                                <div class="link_overlay">
                                     <div class="testimonial">
                                        <div>
                                           <p><strong>Spartans</strong></p>
                                           <p></p>
                                       
                                            <p><a>Conoce más</a></p>
                                        </div>
                                    </div>
                                    <span class="topaz-hover"></span>
                                </div>
                            </div>
                        </div>
                        <!--! PHOTO (1) -->
                        <!-- PHOTO (1) -->
                        <div class="isotope-item">
                            <div class="work-item">
                                <div class="dima-masonry-100"></div>
                                <img src="images/spartans/spartan-04.jpg" alt="Brand Consulting">
                                <div class="link_overlay">
                                     <div class="testimonial">
                                        <div>
                                           <p><strong>Spartans</strong></p>
                                           <p></p>
                                       
                                            <p><a>Conoce más</a></p>
                                        </div>
                                    </div>
                                    <span class="topaz-hover"></span>
                                </div>
                            </div>
                        </div>
                        <!--! PHOTO (1) -->
                        <!-- PHOTO (1) -->
                        <div class="isotope-item">
                            <div class="work-item">
                                <div class="dima-masonry-100"></div>
                                <img src="images/spartans/spartan-05.jpg" alt="Brand Consulting">
                                <div class="link_overlay">
                                     <div class="testimonial">
                                        <div>
                                           <p><strong>Spartans</strong></p>
                                           <p></p>
                                       
                                            <p><a>Conoce más</a></p>
                                        </div>
                                    </div>
                                    <span class="topaz-hover"></span>
                                </div>
                            </div>
                        </div>
                        <!--! PHOTO (1) -->
                    </div>
                    </div>
                </div>
            </section>
            <!--! SCREENSHOTS SECTION -->