<!-- 1)Important in all pages -->
        <script src="js/core/jquery-2.1.1.min.js"></script>
        <!-- 
<script src="http://ajax.googleapis.com/ajax/module/jquery/2.1.3/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/module/jquery-2.1.1.min.js"><\/script>')</script>
 -->
        <script src="js/core/load.js"></script>
        <script src="js/core/jquery.easing.1.3.js"></script>
        <script src="js/core/modernizr-2.8.2.min.js"></script>
        <script src="js/core/imagesloaded.pkgd.min.js"></script>
        <script src="js/core/respond.src.js"></script>

        <script src="js/libs.min.js"></script>
        <!-- ALL THIS FILES CAN BE REPLACE WITH ONE FILE libs.min.js -->
        <!-- 
<script src="js/module/waypoints.min.js"></script>
<script src="js/module/SmoothScroll.js"></script>
<script src="js/module/skrollr.js"></script>
<script src="js/module/sly.min.js"></script>
<script src="js/module/perfect-scrollbar.js"></script>
<script src="js/module/retina.min.js"></script>
<script src="js/module/jquery.localScroll.min.js"></script>
<script src="js/module/jquery.scrollTo-min.js"></script>
<script src="js/module/jquery.nav.js"></script>
<script src="js/module/hoverIntent.js"></script>
<script src="js/module/superfish.js"></script>
<script src="js/module/jquery.placeholder.js"></script>
<script src="js/module/countUp.js"></script>
<script src="js/module/isotope.pkgd.min.js"></script>
<script src="js/module/jquery.flatshadow.js"></script>
<script src="js/module/jquery.knob.js"></script>
<script src="js/module/jflickrfeed.min.js"></script>
<script src="js/module/instagram.min.js"></script>
<script src="js/module/jquery.tweet.js"></script>
<script src="js/module/bootstrap.min.js"></script>
<script src="js/module/bootstrap-transition.js"></script>
<script src="js/module/responsive.tab.js"></script>
<script src="js/module/jquery.magnific-popup.min.js"></script>
<script src="js/module/jquery.validate.min.js"></script>
<script src="js/module/owl.carousel.min.js"></script>
<script src="js/module/jquery.flexslider.js"></script>
<script src="js/module/jquery-ui.min.js"></script>
<script src="js/module/zoomsl-3.0.min.js"></script>
 -->
        <!-- END -->

        <script src="js/specific/mediaelement/mediaelement-and-player.min.js"></script>
        <script src="js/specific/video.js"></script>
        <script src="js/specific/bigvideo.js"></script>

        <!--script src="http://maps.google.com/maps/api/js?sensor=true"></script>
        <script src="js/specific/gmap3.min.js"></script>
        <script src="js/map.js"></script-->

        <script type="text/javascript" src="js/specific/revolution-slider/js/jquery.themepunch.tools.min.js"></script>
        <script type="text/javascript" src="js/specific/revolution-slider/js/jquery.themepunch.revolution.min.js"></script>

        <script src="js/main.js"></script>


        <!--! FOOTER -->