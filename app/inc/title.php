<!-- BIG TITLE HERE -->
            <section class="title_container">
                <div class="page-section-content overflow-hidden">
                    <div class="background-image-hide parallax-background">
                        <img class="background-image" alt="Background Image" src="<?php echo $imagepage;?>">
                    </div>
                    <div class="clear"></div>
                    <div class="container page-section text-center">
                        <h2 class="uppercase undertitle"><?php echo $titlepage;?></h2>
                    </div>
                </div>
            </section>
            <!--! BIG TITLE HERE -->