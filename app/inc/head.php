<head>
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <title><?php echo $title;?></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    <link rel="stylesheet" data-them="" href="css/styles.css">
    <link rel="stylesheet" data-them="" href="css/styles-green-dark.css">
    <link rel="stylesheet" href="fonts/stylesheet.css" type="text/css" charset="utf-8" />
    <style type="text/css">
					h1, h2, h3, h4, h5, h6{
				font-family: 'airstrike_expandedexpanded';
							}
		</style>

    <link rel="stylesheet" type="text/css" href="js/specific/revolution-slider/css/settings.css" media="screen" />
    <!--[if IE]>
       <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
      <![endif]-->

    <!--[if lt IE 9]>
       <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
      <![endif]-->

    <!-- Googl Font -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:100,400,600,700,300" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,300italic,400italic" rel="stylesheet" type="text/css">

    <link rel="shortcut icon" href="images/favicon.png" type="image/x-icon"/>
    <link rel="apple-touch-icon" href="images/favicon.png" type="image/x-icon"/>

    <!--Contact validation-->
    <script src="contact/freecontactformvalidation.js"></script>
        <script>
          required.add('Full_Name','NOT_EMPTY','Full Name');
          required.add('Email_Address','EMAIL','Email Address');
          required.add('Your_Message','NOT_EMPTY','Your Message');
          required.add('AntiSpam','NOT_EMPTY','Anti-Spam Question');
	      </script>
    <!--End Contact validation-->


</head>