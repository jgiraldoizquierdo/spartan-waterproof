<?php

$current_page = basename($_SERVER['PHP_SELF']);

switch($page){
    case 'index':
        $title = 'Bienvenidos a SPARTAN WATERPROOF - INICIO';
        $content = 'app/index.php';
        $imagepage = 'http://fpoimg.com/1920x494';
        $titlepage = "INICIO";
        break;
    case 'compania':
        $title = 'Bienvenidos a SPARTAN WATERPROOF - COMPAÑIA';
        $content = 'app/compania.php';
        $imagepage = 'images/sections/compania.jpg';
        $titlepage = "COMPAÑIA";
        break;
    case 'productos':
        $title = 'Bienvenidos a SPARTAN WATERPROOF - PRODUCTOS';
        $content = 'app/productos.php';
        $imagepage = 'images/sections/productos.jpg';
        $titlepage = "PRODUCTOS";
        break;
    case 'spartans':
        $title = 'Bienvenidos a SPARTAN WATERPROOF - SPARTANS';
        $content = 'pages/contact.php';
        $imagepage = 'http://fpoimg.com/1920x494';
        $titlepage = "SPARTANS";
        break;
    case 'tiendas':
        $title = 'Bienvenidos a SPARTAN WATERPROOF - TIENDAS';
        $content = 'app/contact.php';
        $imagepage = 'http://fpoimg.com/1920x494';
        $titlepage = "TIENDAS";
        break;
    case 'noticias':
        $title = 'Bienvenidos a SPARTAN WATERPROOF - NOTICIAS';
        $content = 'pages/noticias.php';
        $imagepage = 'images/sections/noticias.jpg';
        $titlepage = "NOTICIAS";
        break;
    case 'contactos':
        $title = 'Bienvenidos a SPARTAN WATERPROOF - CONTACTOS';
        $content = 'app/contactos.php';
        $imagepage = 'images/sections/contacto.jpg';
        $titlepage = "CONTACTOS";
        break;
}
//the following html could be in a separate file and included, eg layout.php
?>