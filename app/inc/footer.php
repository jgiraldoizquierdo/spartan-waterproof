<!-- FOOTER -->
            <!--! ALL CONTENTS -->
            <!-- BOTTOM FOOTER -->
            <footer role="contentinfo" class="dima-footer">
                <div class="container">
                    <div class="ok-row">
                        <!-- FOOTER MENU -->
                        <div class="ok-md-7 ok-xsd-12 ok-sd-12 hidden-xsd">
                            <ul class="dima-center-full dima-menu">
                                <li class="<?php if ($current_page == "index.php"){ echo "active "; } ?>">
                                    <a data-animated-link="fadeOut" href="index.php">Home</a>
                                </li>
                                <li class="<?php if ($current_page == "compania.php"){ echo "active "; } ?>">
                                    <a data-animated-link="fadeOut" href="compania.php">Compañia</a>
                                </li>
                                <li class="<?php if ($current_page == "productos.php"){ echo "active "; } ?>">
                                    <a data-animated-link="fadeOut" href="productos.php">Productos</a>
                                </li>
                                <li class="<?php if ($current_page == "spartans.php"){ echo "active "; } ?>">
                                    <a data-animated-link="fadeOut" href="spartans.php">Spartans</a>
                                </li>
                                <!--li class="<?php if ($current_page == "tiendas.php"){ echo "active "; } ?>">
                                    <a data-animated-link="fadeOut" href="tiendas.php">Tiendas</a>
                                </li-->
                                <li class="<?php if ($current_page == "noticias.php"){ echo "active "; } ?>">
                                    <a data-animated-link="fadeOut" href="noticias.php">Noticias</a>
                                </li>
                                <li class="<?php if ($current_page == "contactos.php"){ echo "active "; } ?>">
                                    <a data-animated-link="fadeOut" href="contactos.php">Contactos</a>
                                </li>
                            </ul>
                        </div>
                        <!--! FOOTER MENU -->
                        
                        <!-- COPYRIGHT -->
                        <div class="ok-md-5 ok-xsd-12 ok-sd-12">
                            <div class="dima-center-full network">
                                <div class="widget-content">
                                <div>
                                    <ul class="inline clearfix">
                                        <li>
                                            <a data-animated-link="fadeOut" href="https://www.facebook.com/spartanwaterproof/" target="_blank">
                                                Facebook
                                            </a>
                                        </li>
                                        <li>
                                            <a data-animated-link="fadeOut" href="https://www.instagram.com/spartanwaterproof/">
                                                Instagram
                                            </a>
                                        </li>
                                        <!--li>
                                            <a data-animated-link="fadeOut" href="#">
                                                Youtube
                                            </a>
                                        </li>
                                        <li>
                                            <a data-animated-link="fadeOut" href="#">
                                                Google+
                                            </a>
                                        </li>
                                        <li>
                                            <a data-animated-link="fadeOut" href="#">
                                                Twitter
                                            </a>
                                        </li>
                                        
                                        <li>
                                            <a data-animated-link="fadeOut" href="#">
                                                Instagram
                                            </a>
                                        </li-->
                                    </ul>
                                </div>
                            </div>
                            </div>
                        </div>
                        <!--! COPYRIGHT -->
                    </div>
                </div>
                
                <div class="double-clear"></div>

                <div class="container">
                    <div class="ok-row">
                       
                        <!-- COPYRIGHT -->
                        <div class="ok-md-8 ok-xsd-12 ok-sd-12">
                            <div class="dima-center-full copyright">
                                <p>© 2017 Spartan Waterproof.<span>Teléfono</span> +571 480 2261 <span>Móvil</span> 301 453 07 93
                                </p>
                            </div>
                        </div>
                        <!--! COPYRIGHT -->
                        
                        
                        <!-- FOOTER MENU -->
                        <div class="ok-md-4 ok-xsd-12 ok-sd-12 hidden-xsd">
                            <svg width="200" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 250 37.1"><g fill="#B3B3B3"><path d="M198.5 30h-74.9c-.1 0-1.5-2-4.2-6v-.1h3.9c4-6 6.1-9.4 6.3-10.1H99.1l-.2.1c-5.5 8.5-8.9 13.5-10 15.2 0 .1.1.2.2.2h10.3l.2-.1c4.1-6.7 6.4-10.2 6.8-10.7h9.9v.1c-1 1.7-1.5 2.6-1.7 2.7h-7.3c-.3.3-1.2 1.5-2.6 3.6v.1h5c3.2 5.2 7.2 11.5 7.8 12.3h76.3l4.7-7.3zM200.2 30l-4.7 7.1h1.9l4.7-7.1M204 30l-4.7 7.1h2l4.6-7.1M207.9 30l-4.7 7.1h1.9l4.7-7.1"/><path d="M132.1 13.7h27.5v.2c-1.3 2-2.1 3.3-2.6 4h-9l-6.3 9.5h-9.3v-.2l6-9.4h-9v-.1c1.6-2.4 2.5-3.8 2.7-4zM162.2 13.7h27.5v.2c-5.5 8.6-8.4 13.1-8.7 13.5h-9.3v-.2l2.3-3.6h-8.7l-2.6 3.8h-9.3v-.2c5.7-8.8 8.7-13.3 8.8-13.5zm5.1 6.6h8.7c.2-.1.7-.9 1.7-2.5H169c-.1.1-.6.9-1.7 2.5zM192.4 13.7h9.6l4.3 6.5c2.6-4.2 4-6.3 4.2-6.5h9.3v.2c-5.5 8.6-8.4 13.1-8.7 13.5h-9.6l-4.3-6.5c-2.5 4-3.9 6.2-4.2 6.5h-9.3v-.2c5.6-8.8 8.5-13.3 8.7-13.5zM8.7 13.7h27.5v.2c-1.3 2-2.1 3.3-2.6 4h-18c-.1 0-.3.3-.7 1v.1h18c0 .3-1.8 3.1-5.4 8.5H0v-.2c1.4-2.2 2.3-3.6 2.6-4h18c.1 0 .3-.3.7-1v-.1h-18V22c3.5-5.3 5.3-8.1 5.4-8.3zM69.1 13.7h27.5v.2C91 22.5 88.1 27 87.8 27.4h-9.3v-.2l2.3-3.6h-8.7l-2.6 3.8h-9.3v-.2c5.8-8.8 8.7-13.3 8.9-13.5zm5.1 6.6h8.7c.2-.1.7-.9 1.7-2.5h-8.7c-.1.1-.7.9-1.7 2.5zM38.9 13.7c-.1.1-3.1 4.6-8.7 13.5v.2h9.3l2.6-3.8 2-3.3 2 3.3h14c.4-.4 2.5-3.6 6.4-9.7v-.2H38.9zm13.8 6.6H44c1-1.6 1.6-2.5 1.7-2.5h8.7c-1 1.6-1.5 2.4-1.7 2.5z"/><g><path d="M222 13.8l-5.2 8.2 18.8 8.2h-24l-4.4 6.9h27.9l6.6-9.1-13.1-6 5.6-1 13.8 9-5 7.1 7-3.5V0s-9 13.8-28 13.8z"/><path d="M215.8 23.9l9.6 4.4h-12.5"/></g></g></svg>
                        </div>
                        <!--! FOOTER MENU -->
                        
                        
                    </div>
                </div>
            </footer>
            <!--! BOTTOM FOOTER -->