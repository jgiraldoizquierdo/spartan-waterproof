<!-- HEADER -->
<header role="banner">
    <div class="dima-navbar-wrap desk-nav dima-navbar-fixed-top-active dima-topbar-active">
        <div class="dima-navbar fix-two" data-offsetBy=".tp-banner-container">
            <!-- TOP BAR -->
            <div class="dima-topbar">
                <div class="container">
                    <ul class="float-start text-start dima-menu">
                        <li>
                            <a data-animated-link="fadeOut" href="#">
                                <i class="fa fa-map-marker"></i>Bogotá - COLOMBIA</a>
                        </li>
                        <li>
                            <a data-animated-link="fadeOut" href="tel: 3014530793">
                                <i class="fa fa-phone"></i>301 453 07 93</a>
                        </li>
                    </ul>
                </div>
            </div>
            <!--! TOP BAR -->
            <div class="clearfix dima-nav-fixed"></div>
            <div class="container">
                <!-- LOGO -->
                <div class="logo">
                    <h1>
                                <a data-animated-link="fadeOut" href="index.php" title="PixelDima.com logo">
                                    <span class="vertical-middle"></span>
                                    <svg width="300" height="44" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 250 37.1"><g fill="#BE0811"><path d="M198.5 30h-74.9c-.1 0-1.5-2-4.2-6v-.1h3.9c4-6 6.1-9.4 6.3-10.1H99.1l-.2.1c-5.5 8.5-8.9 13.5-10 15.2 0 .1.1.2.2.2h10.3l.2-.1c4.1-6.7 6.4-10.2 6.8-10.7h9.9v.1c-1 1.7-1.5 2.6-1.7 2.7h-7.3c-.3.3-1.2 1.5-2.6 3.6v.1h5c3.2 5.2 7.2 11.5 7.8 12.3h76.3l4.7-7.3zM200.2 30l-4.7 7.1h1.9l4.7-7.1M204 30l-4.7 7.1h2l4.6-7.1M207.9 30l-4.7 7.1h1.9l4.7-7.1"/></g><path fill="#E6E6E6" d="M132.1 13.7h27.5v.2c-1.3 2-2.1 3.3-2.6 4h-9l-6.3 9.5h-9.3v-.2l6-9.4h-9v-.1c1.6-2.4 2.5-3.8 2.7-4zM162.2 13.7h27.5v.2c-5.5 8.6-8.4 13.1-8.7 13.5h-9.3v-.2l2.3-3.6h-8.7l-2.6 3.8h-9.3v-.2c5.7-8.8 8.7-13.3 8.8-13.5zm5.1 6.6h8.7c.2-.1.7-.9 1.7-2.5H169c-.1.1-.6.9-1.7 2.5zM192.4 13.7h9.6l4.3 6.5c2.6-4.2 4-6.3 4.2-6.5h9.3v.2c-5.5 8.6-8.4 13.1-8.7 13.5h-9.6l-4.3-6.5c-2.5 4-3.9 6.2-4.2 6.5h-9.3v-.2c5.6-8.8 8.5-13.3 8.7-13.5zM8.7 13.7h27.5v.2c-1.3 2-2.1 3.3-2.6 4h-18c-.1 0-.3.3-.7 1v.1h18c0 .3-1.8 3.1-5.4 8.5H0v-.2c1.4-2.2 2.3-3.6 2.6-4h18c.1 0 .3-.3.7-1v-.1h-18V22c3.5-5.3 5.3-8.1 5.4-8.3zM69.1 13.7h27.5v.2C91 22.5 88.1 27 87.8 27.4h-9.3v-.2l2.3-3.6h-8.7l-2.6 3.8h-9.3v-.2c5.8-8.8 8.7-13.3 8.9-13.5zm5.1 6.6h8.7c.2-.1.7-.9 1.7-2.5h-8.7c-.1.1-.7.9-1.7 2.5zM38.9 13.7c-.1.1-3.1 4.6-8.7 13.5v.2h9.3l2.6-3.8 2-3.3 2 3.3h14c.4-.4 2.5-3.6 6.4-9.7v-.2H38.9zm13.8 6.6H44c1-1.6 1.6-2.5 1.7-2.5h8.7c-1 1.6-1.5 2.4-1.7 2.5z"/><g fill="#1A1A1A"><path d="M125.1 35.6h-1v-4.3h1v4.3zm1-4.3c.7 0 1.1.2 1.3.7 0 .1.1.2.1.3v.1c0 .3-.1.5-.4.7l-.1.1h-.1c.1 0 .1.1.2.1.3.2.5.4.5.8v.1c0 .3 0 .4-.1.4 0 .1-.1.2-.2.4s-.4.3-.8.4h-1.3v-.8h.9c.3-.1.5-.2.5-.5V34c0-.2-.1-.3-.2-.4 0 0-.1-.1-.3-.1h-.9v-.8h1c.2-.1.3-.2.3-.4v-.2c-.1-.1-.2-.2-.4-.2h-.9v-.8h.9zM131.1 31.3l1.5 4.3h-1.1l-.3-.9H130l.2-.8h.7l-.4-1.6-.9 3.3h-.9V35l1.2-3.7h1.2zM135.5 32.1c-.2 0-.5.2-.6.4-.2.3-.3.6-.3 1s.1.7.3 1c.2.2.4.4.6.4v.8c-.5 0-1-.2-1.4-.6-.4-.4-.6-.9-.6-1.5s.2-1.1.6-1.5c.4-.4.8-.6 1.4-.6v.6zm2.2 1.4c0 .6-.2 1.1-.6 1.5-.4.4-.8.6-1.4.6v-.8c.3 0 .5-.1.7-.4l.1-.1h-.8v-.8h2zm-1.3-1c-.2-.3-.4-.4-.7-.4v-.8c.5 0 1 .2 1.4.6.2.2.3.4.4.7l-.8.4c-.1-.2-.2-.4-.3-.5zM139.8 34.6c.2.1.4.2.5.2h.1v.8c-.5 0-.9-.1-1.2-.3-.3-.2-.5-.5-.5-1l.9-.1c0 .1.1.3.2.4zm.3-.8c-.9-.2-1.3-.6-1.3-1.2v-.1c0-.7.4-1.1 1.3-1.2h.3v.7c-.2 0-.3 0-.4.1-.2.1-.2.2-.2.4s.2.3.7.4c.1 0 .2 0 .3.1.9.1 1.3.6 1.3 1.3v.1c0 .7-.4 1.1-1.3 1.3h-.3v-.8c.2 0 .3 0 .4-.1.2-.1.2-.2.2-.4s-.2-.4-.7-.5c0-.1-.1-.1-.3-.1zm.5-2.5c.4 0 .8.1 1.1.4.2.2.3.5.4.9l-.5.1h-.4c0-.2-.1-.4-.2-.5-.1-.1-.2-.1-.4-.2v-.7zM147.7 35.6h-.3l-1.6-4.3h.9l.9 2.6v1.7zm1.6-3l.5 1.3v1.7h-.3l-.6-1.7-.6 1.7h-.3V34l.5-1.3h.8zm.7 1.3l1-2.6h.9l-1.6 4.3h-.3v-1.7zM155.2 31.3l1.5 4.3h-1.1l-.3-.9h-1.2l.2-.8h.7l-.4-1.6-.9 3.3h-.9V35l1.2-3.7h1.2zM160.9 31.3v.8h-3.2v-.8h3.2zm-1.1 4.3h-1v-3.3h1v3.3zM163 35.6h-1v-4.3h1v4.3zm2.1-4.3v.8h-2v-.8h2zm-.1 1.6v.8h-1.8v-.8h1.8zm.2 1.9v.8h-2v-.8h2zM167.2 35.6h-1v-4.3h1v4.3zm2-1.9c.1.1.1.2.2.3 0 0 .1.2.1.4v.2c.1.4.1.7.2.8 0 .1.1.2.2.3h-1s-.1-.1-.2-.4c0 0-.1-.3-.2-.8 0-.1-.1-.2-.2-.3-.1 0-.4-.1-.9-.1v-.8h.8c.3-.1.5-.3.5-.6v-.1c-.1-.3-.3-.5-.5-.5h-.7v-.8h.7c.6 0 1 .2 1.2.5.2.3.3.5.3.8v.1c0 .3-.1.6-.3.9-.2 0-.2 0-.2.1zM171.9 35.6h-1v-4.3h1v4.3zm.8-4.3c.6 0 1 .2 1.2.5.2.3.3.6.3.9v.1c0 .3-.1.7-.3.9-.3.3-.8.5-1.5.5h-.4v-.8h.8c.3-.1.5-.3.5-.6v-.1c-.1-.3-.3-.5-.5-.5h-.8v-.8h.7zM176.3 35.6h-1v-4.3h1v4.3zm1.9-1.9c.1.1.1.2.2.3 0 0 .1.2.1.4v.2c.1.4.1.7.2.8 0 .1.1.2.2.3h-1s-.1-.1-.2-.4c0 0-.1-.3-.2-.8 0-.1-.1-.2-.2-.3-.1 0-.4-.1-.9-.1v-.8h.8c.3-.1.5-.3.5-.6v-.1c-.1-.3-.3-.5-.5-.5h-.7v-.8h.7c.6 0 1 .2 1.2.5.2.3.3.5.3.8v.1c0 .3-.1.6-.3.9-.1 0-.1 0-.2.1zM181.9 32.1c-.2 0-.5.2-.6.4-.2.3-.3.6-.3 1s.1.7.3 1c.2.2.4.4.6.4v.8c-.5 0-1-.2-1.4-.6-.4-.4-.6-.9-.6-1.5s.2-1.1.6-1.5c.4-.4.8-.6 1.4-.6v.6zm.2 2.7c.3 0 .5-.1.7-.4.2-.3.3-.6.3-1s-.1-.7-.3-1c-.2-.3-.4-.4-.7-.4v-.8c.5 0 1 .2 1.4.6.4.4.6.9.6 1.5s-.2 1.1-.6 1.5c-.4.4-.8.6-1.4.6v-.6zM187.1 32.1c-.2 0-.5.2-.6.4-.2.3-.3.6-.3 1s.1.7.3 1c.2.2.4.4.6.4v.8c-.5 0-1-.2-1.4-.6-.4-.4-.6-.9-.6-1.5s.2-1.1.6-1.5c.4-.4.8-.6 1.4-.6v.6zm.2 2.7c.3 0 .5-.1.7-.4.2-.3.3-.6.3-1s-.1-.7-.3-1c-.2-.3-.4-.4-.7-.4v-.8c.5 0 1 .2 1.4.6.4.4.6.9.6 1.5s-.2 1.1-.6 1.5c-.4.4-.8.6-1.4.6v-.6zM191.3 35.6h-1v-4.3h1v4.3zm2.2-4.3v.8h-2v-.8h2zm-.2 1.6v.8h-1.8v-.8h1.8z"/></g><g fill="#BE0811"><path d="M222 13.8l-5.2 8.2 18.8 8.2h-24l-4.4 6.9h27.9l6.6-9.1-13.1-6 5.6-1 13.8 9-5 7.1 7-3.5V0s-9 13.8-28 13.8z"/><path d="M215.8 23.9l9.6 4.4h-12.5"/></g></svg>
                                </a>
                            </h1>
                </div>
                <!--! LOGO -->
                <!-- NAVBAR BUTTON -->
                <a class="dima-btn-nav" href="#">
                    <i class="fa fa-bars"></i>
                </a>
                <!--! NAVBAR BUTTON -->
                <nav role="navigation">
                    <!-- MENU -->
                    <ul class="dima-nav  ">
                        <li class="sub-icon menu-item-has-children <?php if ($current_page == " index.php "){ echo "active "; } ?>">
                            <a data-animated-link="fadeOut" href="index.php">Home</a>
                        </li>
                        <li class="sub-icon menu-item-has-children <?php if ($current_page == " compania.php "){ echo "active "; } ?>">
                            <a data-animated-link="fadeOut" href="compania.php">Compañia</a>
                        </li>
                        <li class="sub-icon menu-item-has-children menu-full-width <?php if ($current_page == " productos.php "){ echo "active "; } else if ($current_page == "bag.php "){ echo "active "; } else if ($current_page == "sidebag.php "){ echo "active "; } else if ($current_page == "cyli.php "){ echo "active "; } ?>">
                            <a data-animated-link="fadeOut" href="productos.php">Productos</a>
                            <ul class="sub-menu nav-menu container">
                                <li>
                                    <!--! CLIENTS SECTION -->
                                    <section class="section section-colored" data-bg="#f5f5f5">
                                        <div class="container">
                                            <div class="clients-wrapper">
                                                <div class="boxed-blog owl-carousel owl-darck" data-owl-namber="3" data-owl-phone="1" data-owl-tablet="4" data-owl-margin="3" data-owl-autoPlay="true" data-owl-loop="true" data-owl-navigation="false" data-owl-pagination="false">
                                                    <div class="client">
                                                        <span class="div-title-left">
                                    <h5>BAG LUGGAGE</h5>
                                    <h6 class="theme-color">Waterproof</h6>
                                    <a href="bagluggage.php" ><h6 class="theme-color">Ver más</h6></a>
                                </span>
                                                        <a data-animated-link="fadeOut" href="bagluggage.php" class="div-title-right">
                                                            <img src="images/productos/mini-foto-01.jpg" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="client">
                                                        <span class="div-title-left">
                                    <h5>DRYBAG</h5>
                                    <h6 class="theme-color">Waterproof</h6>
                                    <a href="drybag.php" ><h6 class="theme-color">Ver más</h6></a>
                                </span>
                                                        <a data-animated-link="fadeOut" href="drybag.php" class="div-title-right">
                                                            <img src="images/productos/mini-foto-02.jpg" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="client">
                                                        <span class="div-title-left">
                                    <h5>ROLL BAG</h5>
                                    <h6 class="theme-color">Waterproof</h6>
                                    <a href="rollbag.php" ><h6 class="theme-color">Ver más</h6></a>
                                </span>
                                                        <a data-animated-link="fadeOut" href="rollbag.php" class="div-title-right">
                                                            <img src="images/productos/mini-foto-03.jpg" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="client">
                                                        <span class="div-title-left">
                                    <h5>TRUNK BAG</h5>
                                    <h6 class="theme-color">Waterproof</h6>
                                    <a href="trunkbag.php"><h6>Ver más</h6></a>
                                </span>
                                                        <a data-animated-link="fadeOut" href="trunkbag.php" class="div-title-right">
                                                            <img src="images/productos/mini-foto-04.jpg" alt="">
                                                        </a>
                                                    </div>

                                                </div>
                                                <div class="prev-next">
                                                    <a class="prev"></a>
                                                    <a class="next"></a>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                    <!--! CLIENTS SECTION -->
                                </li>
                            </ul>
                        </li>
                        <li class="sub-icon menu-item-has-children <?php if ($current_page == " spartans.php "){ echo "active "; } ?>">
                            <a data-animated-link="fadeOut" href="spartans.php">Spartans</a>
                        </li>
                        <!--li class="sub-icon menu-item-has-children <?php if ($current_page == " tiendas.php "){ echo "active "; } ?>">
                            <a data-animated-link="fadeOut" href="tiendas.php">Tiendas</a>

                        </li-->
                        <li class="sub-icon menu-item-has-children <?php if ($current_page == " noticias.php "){ echo "active "; } ?>">
                            <a data-animated-link="fadeOut" href="noticias.php">Noticias</a>
                        </li>
                        <li class="sub-icon menu-item-has-children menu-full-width <?php if ($current_page == " contactos.php "){ echo "active "; }?>">
                            <a data-animated-link="fadeOut" href="contactos.php">Contactos</a>
                            <ul class="sub-menu nav-menu  ">
                                <li class="sub-icon menu-item-has-children">
                                    <!--contact-->
                                    <!-- Success Msg -->
                                    <div id="contactSuccess" class="dima-alert dima-alert-success fade in hide">
                                        <button type="button" class="close" data-dismiss="alert">×</button>
                                        <i class="fa  fa-check "></i>
                                        <h4 class="header-alert">Success</h4>
                                        <p>Your message has been sent to us.</p>
                                    </div>
                                    <!--! Success Msg -->
                                    <!-- Error Msg -->
                                    <div id="contactError" class="dima-alert dima-alert-error fade in hide">
                                        <button type="button" class="close" data-dismiss="alert">×</button>
                                        <i class="fa fa-times "></i>
                                        <h4 class="header-alert">Error!</h4>
                                        <p> There was an error sending your message.</p>
                                    </div>
                                    <!--! Error Msg -->
                                    <!-- Form -->
                                    <form name="freecontactform" method="post" action="contact/freecontactformprocess.php" onsubmit="return validate.check(this)">
                                        <div class="ok-row">
                                            <div class="post ok-md-4 ok-xsd-12  ">
                                                <div class="field">
                                                    <input type="text" name="Full_Name" id="Full_Name" maxlength="80">
                                                </div>
                                                <div class="field">
                                                    <input type="text" name="Email_Address" id="Email_Address" maxlength="100">
                                                </div>
                                                <div class="field">
                                                    <input type="text" name="Telephone_Number" id="Telephone_Number" maxlength="100">
                                                </div>
                                            </div>
                                            <div class="post ok-md-8 ok-xsd-12  ">
                                                <div class="field  ">
                                                    <textarea name="Your_Message" id="Your_Message" maxlength="2000" class="textarea"></textarea>
                                                </div>
                                                <input type="submit" value="ENVIAR" class="no-rounded button small fill">
                                            </div>
                                        </div>
                                        <div class="ok-row">
                                        <div class="post ok-md-6 ok-xsd-12">
                                            <!-- CONTACT INFO -->
                                            <div class="widget">
                                                <h6 class="widget-title uppercase">Comunicate con nosotros</h6>
                                                <h3 class="widget-title uppercase"><a href="tel:301 453 07 93">301 453 07 93</a></h3>
                                            </div>
                                            <!--! CONTACT INFO -->
                                        </div>
                                        <div class="post ok-md-6 ok-xsd-12">
                                           <h6 class="widget-title uppercase">Siguenos en nuestras redes sociales</h6>
                                            <aside role="complementary">

                                                <!-- SOCIAL NETWORK -->
                                                <div class="widget">
                                                    <div class="widget-content">
                                                        <ul class="social-media social-medium inline clearfix">
                                                            <li>
                                                                <a data-animated-link="fadeOut" href="https://www.facebook.com/spartanwaterproof/" target="_blank">
                                                                    <i class="fa fa-facebook"></i>
                                                                </a>
                                                            </li>
                                                            <li>
                                    <a data-animated-link="fadeOut" href="https://www.instagram.com/spartanwaterproof/">
                                        <i class="fa fa-instagram"></i>
                                    </a>
                                </li>
                                                            <!--li>
                                                                <a data-animated-link="fadeOut" href="#">
                                                                    <i class="fa fa-twitter"></i>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a data-animated-link="fadeOut" href="#">
                                                                    <i class="fa fa-google-plus"></i>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a data-animated-link="fadeOut" href="#">
                                                                    <i class="fa fa-instagram"></i>
                                                                </a>
                                                            </li-->
                                                        </ul>
                                                    </div>
                                                </div>
                                                <!--! SOCIAL NETWORK -->
                                            </aside>
                                        </div>
                                    </div>
                                    </form>
                                    
                                    <!--! Form -->
                                    <!--contact-->
                                </li>
                            </ul>
                        </li>
                    </ul>
                    <!--! MENU -->
                </nav>
            </div>
            <!-- SEARCH BOX -->
            <div id="search-box">
                <div class="container">
                    <form>
                        <input type="text" placeholder="Start Typing...">
                    </form>
                    <div id="close">
                        <a data-animated-link="fadeOut" href="#">
                            <i class="di-close"></i>
                        </a>
                    </div>
                </div>
            </div>
            <!--! SEARCH BOX -->
        </div>
        <div class="clear-nav"></div>
    </div>
    <!-- PHONE MENU -->
    <div class="dima-navbar-wrap mobile-nav">
        <div class="dima-navbar fix-one">
            <!-- TOP BAR -->
            <div class="dima-topbar">
                <div class="container">
                    <ul class="float-start text-start dima-menu">
                        <li>
                            <a data-animated-link="fadeOut" href="#">
                                <i class="fa fa-map-marker"></i>Bogotá - COLOMBIA</a>
                        </li>
                        <li>
                            <a data-animated-link="fadeOut" href="#">
                                <i class="fa fa-phone"></i>+213 2020 555013</a>
                        </li>
                    </ul>
                </div>
            </div>
            <!--! TOP BAR -->
            <div class="clearfix dima-nav-fixed"></div>
            <div class="container">
                <!-- Nav bar button -->
                <a class="dima-btn-nav" href="#">
                    <i class="fa fa-bars"></i>
                </a>
                <!-- LOGO -->
                <div class="logo">
                    <h1>
                                <a data-animated-link="fadeOut" href="index.html" title="PixelDima.com logo">
                                    <span class="vertical-middle"></span>
                                    <svg width="180" height="27" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 250 37.1"><g fill="#BE0811"><path d="M198.5 30h-74.9c-.1 0-1.5-2-4.2-6v-.1h3.9c4-6 6.1-9.4 6.3-10.1H99.1l-.2.1c-5.5 8.5-8.9 13.5-10 15.2 0 .1.1.2.2.2h10.3l.2-.1c4.1-6.7 6.4-10.2 6.8-10.7h9.9v.1c-1 1.7-1.5 2.6-1.7 2.7h-7.3c-.3.3-1.2 1.5-2.6 3.6v.1h5c3.2 5.2 7.2 11.5 7.8 12.3h76.3l4.7-7.3zM200.2 30l-4.7 7.1h1.9l4.7-7.1M204 30l-4.7 7.1h2l4.6-7.1M207.9 30l-4.7 7.1h1.9l4.7-7.1"/></g><path fill="#E6E6E6" d="M132.1 13.7h27.5v.2c-1.3 2-2.1 3.3-2.6 4h-9l-6.3 9.5h-9.3v-.2l6-9.4h-9v-.1c1.6-2.4 2.5-3.8 2.7-4zM162.2 13.7h27.5v.2c-5.5 8.6-8.4 13.1-8.7 13.5h-9.3v-.2l2.3-3.6h-8.7l-2.6 3.8h-9.3v-.2c5.7-8.8 8.7-13.3 8.8-13.5zm5.1 6.6h8.7c.2-.1.7-.9 1.7-2.5H169c-.1.1-.6.9-1.7 2.5zM192.4 13.7h9.6l4.3 6.5c2.6-4.2 4-6.3 4.2-6.5h9.3v.2c-5.5 8.6-8.4 13.1-8.7 13.5h-9.6l-4.3-6.5c-2.5 4-3.9 6.2-4.2 6.5h-9.3v-.2c5.6-8.8 8.5-13.3 8.7-13.5zM8.7 13.7h27.5v.2c-1.3 2-2.1 3.3-2.6 4h-18c-.1 0-.3.3-.7 1v.1h18c0 .3-1.8 3.1-5.4 8.5H0v-.2c1.4-2.2 2.3-3.6 2.6-4h18c.1 0 .3-.3.7-1v-.1h-18V22c3.5-5.3 5.3-8.1 5.4-8.3zM69.1 13.7h27.5v.2C91 22.5 88.1 27 87.8 27.4h-9.3v-.2l2.3-3.6h-8.7l-2.6 3.8h-9.3v-.2c5.8-8.8 8.7-13.3 8.9-13.5zm5.1 6.6h8.7c.2-.1.7-.9 1.7-2.5h-8.7c-.1.1-.7.9-1.7 2.5zM38.9 13.7c-.1.1-3.1 4.6-8.7 13.5v.2h9.3l2.6-3.8 2-3.3 2 3.3h14c.4-.4 2.5-3.6 6.4-9.7v-.2H38.9zm13.8 6.6H44c1-1.6 1.6-2.5 1.7-2.5h8.7c-1 1.6-1.5 2.4-1.7 2.5z"/><g fill="#1A1A1A"><path d="M125.1 35.6h-1v-4.3h1v4.3zm1-4.3c.7 0 1.1.2 1.3.7 0 .1.1.2.1.3v.1c0 .3-.1.5-.4.7l-.1.1h-.1c.1 0 .1.1.2.1.3.2.5.4.5.8v.1c0 .3 0 .4-.1.4 0 .1-.1.2-.2.4s-.4.3-.8.4h-1.3v-.8h.9c.3-.1.5-.2.5-.5V34c0-.2-.1-.3-.2-.4 0 0-.1-.1-.3-.1h-.9v-.8h1c.2-.1.3-.2.3-.4v-.2c-.1-.1-.2-.2-.4-.2h-.9v-.8h.9zM131.1 31.3l1.5 4.3h-1.1l-.3-.9H130l.2-.8h.7l-.4-1.6-.9 3.3h-.9V35l1.2-3.7h1.2zM135.5 32.1c-.2 0-.5.2-.6.4-.2.3-.3.6-.3 1s.1.7.3 1c.2.2.4.4.6.4v.8c-.5 0-1-.2-1.4-.6-.4-.4-.6-.9-.6-1.5s.2-1.1.6-1.5c.4-.4.8-.6 1.4-.6v.6zm2.2 1.4c0 .6-.2 1.1-.6 1.5-.4.4-.8.6-1.4.6v-.8c.3 0 .5-.1.7-.4l.1-.1h-.8v-.8h2zm-1.3-1c-.2-.3-.4-.4-.7-.4v-.8c.5 0 1 .2 1.4.6.2.2.3.4.4.7l-.8.4c-.1-.2-.2-.4-.3-.5zM139.8 34.6c.2.1.4.2.5.2h.1v.8c-.5 0-.9-.1-1.2-.3-.3-.2-.5-.5-.5-1l.9-.1c0 .1.1.3.2.4zm.3-.8c-.9-.2-1.3-.6-1.3-1.2v-.1c0-.7.4-1.1 1.3-1.2h.3v.7c-.2 0-.3 0-.4.1-.2.1-.2.2-.2.4s.2.3.7.4c.1 0 .2 0 .3.1.9.1 1.3.6 1.3 1.3v.1c0 .7-.4 1.1-1.3 1.3h-.3v-.8c.2 0 .3 0 .4-.1.2-.1.2-.2.2-.4s-.2-.4-.7-.5c0-.1-.1-.1-.3-.1zm.5-2.5c.4 0 .8.1 1.1.4.2.2.3.5.4.9l-.5.1h-.4c0-.2-.1-.4-.2-.5-.1-.1-.2-.1-.4-.2v-.7zM147.7 35.6h-.3l-1.6-4.3h.9l.9 2.6v1.7zm1.6-3l.5 1.3v1.7h-.3l-.6-1.7-.6 1.7h-.3V34l.5-1.3h.8zm.7 1.3l1-2.6h.9l-1.6 4.3h-.3v-1.7zM155.2 31.3l1.5 4.3h-1.1l-.3-.9h-1.2l.2-.8h.7l-.4-1.6-.9 3.3h-.9V35l1.2-3.7h1.2zM160.9 31.3v.8h-3.2v-.8h3.2zm-1.1 4.3h-1v-3.3h1v3.3zM163 35.6h-1v-4.3h1v4.3zm2.1-4.3v.8h-2v-.8h2zm-.1 1.6v.8h-1.8v-.8h1.8zm.2 1.9v.8h-2v-.8h2zM167.2 35.6h-1v-4.3h1v4.3zm2-1.9c.1.1.1.2.2.3 0 0 .1.2.1.4v.2c.1.4.1.7.2.8 0 .1.1.2.2.3h-1s-.1-.1-.2-.4c0 0-.1-.3-.2-.8 0-.1-.1-.2-.2-.3-.1 0-.4-.1-.9-.1v-.8h.8c.3-.1.5-.3.5-.6v-.1c-.1-.3-.3-.5-.5-.5h-.7v-.8h.7c.6 0 1 .2 1.2.5.2.3.3.5.3.8v.1c0 .3-.1.6-.3.9-.2 0-.2 0-.2.1zM171.9 35.6h-1v-4.3h1v4.3zm.8-4.3c.6 0 1 .2 1.2.5.2.3.3.6.3.9v.1c0 .3-.1.7-.3.9-.3.3-.8.5-1.5.5h-.4v-.8h.8c.3-.1.5-.3.5-.6v-.1c-.1-.3-.3-.5-.5-.5h-.8v-.8h.7zM176.3 35.6h-1v-4.3h1v4.3zm1.9-1.9c.1.1.1.2.2.3 0 0 .1.2.1.4v.2c.1.4.1.7.2.8 0 .1.1.2.2.3h-1s-.1-.1-.2-.4c0 0-.1-.3-.2-.8 0-.1-.1-.2-.2-.3-.1 0-.4-.1-.9-.1v-.8h.8c.3-.1.5-.3.5-.6v-.1c-.1-.3-.3-.5-.5-.5h-.7v-.8h.7c.6 0 1 .2 1.2.5.2.3.3.5.3.8v.1c0 .3-.1.6-.3.9-.1 0-.1 0-.2.1zM181.9 32.1c-.2 0-.5.2-.6.4-.2.3-.3.6-.3 1s.1.7.3 1c.2.2.4.4.6.4v.8c-.5 0-1-.2-1.4-.6-.4-.4-.6-.9-.6-1.5s.2-1.1.6-1.5c.4-.4.8-.6 1.4-.6v.6zm.2 2.7c.3 0 .5-.1.7-.4.2-.3.3-.6.3-1s-.1-.7-.3-1c-.2-.3-.4-.4-.7-.4v-.8c.5 0 1 .2 1.4.6.4.4.6.9.6 1.5s-.2 1.1-.6 1.5c-.4.4-.8.6-1.4.6v-.6zM187.1 32.1c-.2 0-.5.2-.6.4-.2.3-.3.6-.3 1s.1.7.3 1c.2.2.4.4.6.4v.8c-.5 0-1-.2-1.4-.6-.4-.4-.6-.9-.6-1.5s.2-1.1.6-1.5c.4-.4.8-.6 1.4-.6v.6zm.2 2.7c.3 0 .5-.1.7-.4.2-.3.3-.6.3-1s-.1-.7-.3-1c-.2-.3-.4-.4-.7-.4v-.8c.5 0 1 .2 1.4.6.4.4.6.9.6 1.5s-.2 1.1-.6 1.5c-.4.4-.8.6-1.4.6v-.6zM191.3 35.6h-1v-4.3h1v4.3zm2.2-4.3v.8h-2v-.8h2zm-.2 1.6v.8h-1.8v-.8h1.8z"/></g><g fill="#BE0811"><path d="M222 13.8l-5.2 8.2 18.8 8.2h-24l-4.4 6.9h27.9l6.6-9.1-13.1-6 5.6-1 13.8 9-5 7.1 7-3.5V0s-9 13.8-28 13.8z"/><path d="M215.8 23.9l9.6 4.4h-12.5"/></g></svg>
                                </a>
                            </h1>
                </div>
                <!-- MENU -->
                <nav role="navigation" class="clearfix">
                    <ul class="dima-nav  ">
                        <li class="dima-mega-menu col-4 sub-icon menu-item-has-children menu-full-width <?php if ($current_page == " index.php "){ echo "active "; } ?>">
                            <a data-animated-link="fadeOut" href="index.php">Home</a>
                            <ul class="sub-menu nav-menu container box-with-marge">
                                <li class="menu-section">


                                </li>
                            </ul>
                        </li>
                        <li class="sub-icon menu-item-has-children <?php if ($current_page == " compania.php "){ echo "active "; } ?>">
                            <a data-animated-link="fadeOut" href="#">Compañia</a>
                            <ul class="sub-menu nav-menu  ">
                                <li class="sub-icon menu-item-has-children">

                                </li>
                            </ul>
                        </li>
                        <li class="sub-icon menu-item-has-children <?php if ($current_page == " productos.php "){ echo "active "; } ?>">
                            <a data-animated-link="fadeOut" href="productos.php">Productos</a>
                            <ul class="sub-menu nav-menu  ">
                                <li class="sub-icon menu-item-has-children">
                                </li>
                            </ul>
                        </li>
                        <li class="dima-mega-menu col-4 sub-icon menu-item-has-children menu-full-width <?php if ($current_page == " spartans.php "){ echo "active "; } ?>">
                            <a data-animated-link="fadeOut" href="spartans.php">Spartans</a>
                            <ul class="sub-menu nav-menu container">
                                <li>

                                </li>
                            </ul>
                        </li>
                        <li class="sub-icon menu-item-has-children <?php  if ($current_page == " tiendas.php "){ echo "active "; } ?>">
                            <a data-animated-link="fadeOut" href="tiendas.php">Tiendas</a>
                            <ul class="sub-menu nav-menu  ">
                                <li>
                                </li>
                            </ul>
                        </li>
                        <li class="sub-icon menu-item-has-children <?php if ($current_page == " noticias.php "){ echo "active "; } ?>">
                            <a data-animated-link="fadeOut" href="noticias.php">Noticias</a>
                            <ul class="sub-menu nav-menu  ">
                                <li class="sub-icon menu-item-has-children">

                                </li>
                            </ul>
                        </li>
                        <li class="sub-icon menu-item-has-children <?php if ($current_page == " contactos.php "){ echo "active "; }?>">
                            <a data-animated-link="fadeOut" href="contactos.php">Contactos</a>
                            <ul class="sub-menu nav-menu  ">
                                <li class="sub-icon menu-item-has-children">

                                </li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
            <!-- container -->
            <div id="search-box">
                <div class="container">
                    <form>
                        <input type="text" placeholder="Start Typing...">
                    </form>
                    <div id="close">
                        <a data-animated-link="fadeOut" href="#">
                            <i class="di-close"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- !PHONE MENU -->
</header>
<!--! HEADER -->