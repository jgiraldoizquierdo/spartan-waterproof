<?php $page = isset($_GET['menu'])?$_GET['menu']:'noticias'; ?>

<?php include 'inc/configuracion.php'; ?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->

<?php include 'inc/head.php'; ?>

<body class="responsive">

    <!-- LOADING -->
    <div class="all_content loading">

        <?php include 'inc/header.php'; ?>
        
        <?php include 'inc/title.php'; ?>

        <!-- ALL CONTENTS -->
        <div class="dima-main">
        <!-- TEAM INFO -->
            <section class="section section-colored" data-bg="#fafafa" id="about">
                <div class="page-section-content overflow-hidden">
                    <div class="container text-center">
                        <div class="ok-row">
                            <!-- TITLE -->
                            <!-- IMAGEN -->
                            <div class="ok-md-12 ok-xsd-12">
                               <img src="images/noticias/noticia-02.jpg" alt="">
                            </div>
                            <!--! IMAGEN -->
                            <!--! TITLE -->
                            <div class="double-clear"></div>
                            <div class="topaz-line">
                                <i class="di-separator"></i>
                            </div>
                            <div class="ok-md-12 ok-xsd-12 text-start">
                                <h4 class="uppercase" data-animate="fadeInUp" data-delay="200">Encuentrenos en la fería de las dos ruedas en medellín.</h4>
                                <p data-animate="fadeInUp" data-delay="220">A partir del 3 hasta el 6 de mayo encuentre nuestros productos en la feria de las dos ruedas en la ciudad de Medellín en la Plaza Mayor de Exposiciones en
 la Calle 41 No. 55-35 de 10:00 a.m. a 8:00 p.m., stand 547.</p>
                                <div class="double-clear"></div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </section>
            <!--! TEAM INFO -->
        </div>
        <!--! ALL CONTENTS -->

        <?php include 'inc/footer.php'; ?>

        <?php include 'inc/script.php'; ?>

    </div>
    <!--! LOADING -->

</body>

</html>
