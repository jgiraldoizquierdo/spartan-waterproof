<?php $page = isset($_GET['menu'])?$_GET['menu']:'spartans'; ?>

<?php include 'inc/configuracion.php'; ?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->

<?php include 'inc/head.php'; ?>

<body class="responsive">

    <!-- LOADING -->
    <div class="all_content loading">

        <?php include 'inc/header.php'; ?>
        
        <?php include 'inc/title.php'; ?>

        <!-- ALL CONTENTS -->
        <div class="dima-main">
            <!-- SECTION -->
            <section class="section ">
                <div class="page-section-content overflow-hidden">
                    <div class="container">
                        <div class=" dima-main">
                            <div class="ok-row">
                                <!-- LEFT AND RIGHT IMAGE -->
                                <div class=" ok-md-6 ok-xsd-12">
                                    <!-- WITHOUT BORDER AND LEFT IMAGE -->
                                    <div class="dima-testimonial quote-start">
                                        <div class="dima-testimonial-image">
                                            <img src="http://fpoimg.com/80x80" alt="">
                                        </div>
                                        <blockquote>
                                            <div class="quote-content">
                                                <p>Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin ...
                                                </p>
                                                <span class="dima-testimonial-meta">
                                                    <strong>#pedromotero</strong>
                                                    <span>
                                                        <span>|</span>Ingeniero motero</span>
                                                </span>
                                            </div>
                                        </blockquote>
                                    </div>
                                    <!--! WITHOUT BORDER AND LEFT IMAGE -->
                                    <div class="divider">
                                        <div class="dashed"></div>
                                    </div>
                                </div>
                                <div class=" ok-md-6 ok-xsd-12">
                                    <!-- WITHOUT BORDER AND RIGHT IMAGE -->
                                    <div class="dima-testimonial quote-end">
                                        <div class="dima-testimonial-image ">
                                            <img src="http://fpoimg.com/80x80" alt="">
                                        </div>
                                        <blockquote>
                                            <div class="quote-content">
                                                <p>Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin ...
                                                </p>
                                                <span class="dima-testimonial-meta">
                                                    <strong>#pedromotero</strong>
                                                    <span>
                                                        <span>|</span>Ingeniero motero</span>
                                                </span>
                                            </div>
                                        </blockquote>
                                    </div>
                                    <!--! WITHOUT BORDER AND RIGHT IMAGE -->
                                    <div class="divider">
                                        <div class="dashed"></div>
                                    </div>
                                </div>
                                <!--! LEFT AND RIGHT IMAGE -->
                                <!-- LEFT AND RIGHT IMAGE -->
                                <div class=" ok-md-6 ok-xsd-12">
                                    <!-- WITHOUT BORDER AND LEFT IMAGE -->
                                    <div class="dima-testimonial quote-start">
                                        <div class="dima-testimonial-image">
                                            <img src="http://fpoimg.com/80x80" alt="">
                                        </div>
                                        <blockquote>
                                            <div class="quote-content">
                                                <p>Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin ...
                                                </p>
                                                <span class="dima-testimonial-meta">
                                                    <strong>#pedromotero</strong>
                                                    <span>
                                                        <span>|</span>Ingeniero motero</span>
                                                </span>
                                            </div>
                                        </blockquote>
                                    </div>
                                    <!--! WITHOUT BORDER AND LEFT IMAGE -->
                                    <div class="divider">
                                        <div class="dashed"></div>
                                    </div>
                                </div>
                                <div class=" ok-md-6 ok-xsd-12">
                                    <!-- WITHOUT BORDER AND RIGHT IMAGE -->
                                    <div class="dima-testimonial quote-end">
                                        <div class="dima-testimonial-image ">
                                            <img src="http://fpoimg.com/80x80" alt="">
                                        </div>
                                        <blockquote>
                                            <div class="quote-content">
                                                <p>Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin ...
                                                </p>
                                                <span class="dima-testimonial-meta">
                                                    <strong>#pedromotero</strong>
                                                    <span>
                                                        <span>|</span>Ingeniero motero</span>
                                                </span>
                                            </div>
                                        </blockquote>
                                    </div>
                                    <!--! WITHOUT BORDER AND RIGHT IMAGE -->
                                    <div class="divider">
                                        <div class="dashed"></div>
                                    </div>
                                </div>
                                <!--! LEFT AND RIGHT IMAGE -->
                                <!-- LEFT AND RIGHT IMAGE -->
                                <div class=" ok-md-6 ok-xsd-12">
                                    <!-- WITHOUT BORDER AND LEFT IMAGE -->
                                    <div class="dima-testimonial quote-start">
                                        <div class="dima-testimonial-image">
                                            <img src="http://fpoimg.com/80x80" alt="">
                                        </div>
                                        <blockquote>
                                            <div class="quote-content">
                                                <p>Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin ...
                                                </p>
                                                <span class="dima-testimonial-meta">
                                                    <strong>#pedromotero</strong>
                                                    <span>
                                                        <span>|</span>Ingeniero motero</span>
                                                </span>
                                            </div>
                                        </blockquote>
                                    </div>
                                    <!--! WITHOUT BORDER AND LEFT IMAGE -->
                                    <div class="divider">
                                        <div class="dashed"></div>
                                    </div>
                                </div>
                                <div class=" ok-md-6 ok-xsd-12">
                                    <!-- WITHOUT BORDER AND RIGHT IMAGE -->
                                    <div class="dima-testimonial quote-end">
                                        <div class="dima-testimonial-image ">
                                            <img src="http://fpoimg.com/80x80" alt="">
                                        </div>
                                        <blockquote>
                                            <div class="quote-content">
                                                <p>Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin ...
                                                </p>
                                                <span class="dima-testimonial-meta">
                                                    <strong>#pedromotero</strong>
                                                    <span>
                                                        <span>|</span>Ingeniero motero</span>
                                                </span>
                                            </div>
                                        </blockquote>
                                    </div>
                                    <!--! WITHOUT BORDER AND RIGHT IMAGE -->
                                    <div class="divider">
                                        <div class="dashed"></div>
                                    </div>
                                </div>
                                <!--! LEFT AND RIGHT IMAGE -->
                                <!-- LEFT AND RIGHT IMAGE -->
                                <div class=" ok-md-6 ok-xsd-12">
                                    <!-- WITHOUT BORDER AND LEFT IMAGE -->
                                    <div class="dima-testimonial quote-start">
                                        <div class="dima-testimonial-image">
                                            <img src="http://fpoimg.com/80x80" alt="">
                                        </div>
                                        <blockquote>
                                            <div class="quote-content">
                                                <p>Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin ...
                                                </p>
                                                <span class="dima-testimonial-meta">
                                                    <strong>#pedromotero</strong>
                                                    <span>
                                                        <span>|</span>Ingeniero motero</span>
                                                </span>
                                            </div>
                                        </blockquote>
                                    </div>
                                    <!--! WITHOUT BORDER AND LEFT IMAGE -->
                                    <div class="divider">
                                        <div class="dashed"></div>
                                    </div>
                                </div>
                                <div class=" ok-md-6 ok-xsd-12">
                                    <!-- WITHOUT BORDER AND RIGHT IMAGE -->
                                    <div class="dima-testimonial quote-end">
                                        <div class="dima-testimonial-image ">
                                            <img src="http://fpoimg.com/80x80" alt="">
                                        </div>
                                        <blockquote>
                                            <div class="quote-content">
                                                <p>Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin ...
                                                </p>
                                                <span class="dima-testimonial-meta">
                                                    <strong>#pedromotero</strong>
                                                    <span>
                                                        <span>|</span>Ingeniero motero</span>
                                                </span>
                                            </div>
                                        </blockquote>
                                    </div>
                                    <!--! WITHOUT BORDER AND RIGHT IMAGE -->
                                    <div class="divider">
                                        <div class="dashed"></div>
                                    </div>
                                </div>
                                <!--! LEFT AND RIGHT IMAGE -->
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--! SECTION -->
        </div>
        <!--! ALL CONTENTS -->

        <?php include 'inc/footer.php'; ?>

        <?php include 'inc/script.php'; ?>

    </div>
    <!--! LOADING -->

</body>

</html>
