<?php $page = isset($_GET['menu'])?$_GET['menu']:'noticias'; ?>

<?php include 'inc/configuracion.php'; ?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->

<?php include 'inc/head.php'; ?>

<body class="responsive">

    <!-- LOADING -->
    <div class="all_content loading">

        <?php include 'inc/header.php'; ?>
        
        <?php include 'inc/title.php'; ?>

        <!-- ALL CONTENTS -->
        <div class="dima-main">
        <!-- TEAM INFO -->
            <section class="section section-colored" data-bg="#fafafa" id="about">
                <div class="page-section-content overflow-hidden">
                    <div class="container text-center">
                        <div class="ok-row">
                            <!-- TITLE -->
                            <!-- IMAGEN -->
                            <div class="ok-md-12 ok-xsd-12">
                               <img src="images/noticias/noticia-01.jpg" alt="">
                            </div>
                            <!--! IMAGEN -->
                            <!--! TITLE -->
                            <div class="double-clear"></div>
                            <div class="topaz-line">
                                <i class="di-separator"></i>
                            </div>
                            <div class="ok-md-12 ok-xsd-12 text-start">
                                <h4 class="uppercase" data-animate="fadeInUp" data-delay="200">1r Rally de regularidad de Brutosperodecicido</h4>
                                <p data-animate="fadeInUp" data-delay="220">Compartimos con ustedes nuestra grata experiencia como patrocinadores del 1r Rally de regularidad de Brutosperodecicidos. Una vez más probando que nada más resistente y guerrero que Spartan Waterproof.</p>
                                <div class="double-clear"></div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </section>
            <!--! TEAM INFO -->
        </div>
        <!--! ALL CONTENTS -->

        <?php include 'inc/footer.php'; ?>

        <?php include 'inc/script.php'; ?>

    </div>
    <!--! LOADING -->

</body>

</html>
