<?php $page = isset($_GET['menu'])?$_GET['menu']:'tiendas'; ?>

<?php include 'inc/configuracion.php'; ?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->

<?php include 'inc/head.php'; ?>

<body class="responsive">

    <!-- LOADING -->
    <div class="all_content loading">

        <?php include 'inc/header.php'; ?>
        
        <?php include 'inc/title.php'; ?>

        <!-- ALL CONTENTS -->
        <div class="dima-main">
            
            <div class="container">
                    <div class="double-clear"></div>
                    <div class="double-clear"></div>
                        <div class="ok-row">
                            <div class=" ok-md-3 ok-xsd-12 ok-sd-6 services  text-center image">
                                <header role="banner">
                                    <div class="thumb overlay">
                                        <img src="images/works/construction-feature-1.png" alt="Responsive & Mobile">
                                    </div>
                                    <p class="uppercase"><strong>Moteros</strong></p>
                                </header>
                                <p class="withpadding">Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis
                                    bibendum auctor, nisi elit consequat.</p>
                            </div>
                            <div class=" ok-md-3 ok-xsd-12 ok-sd-6 services text-center image">
                                <header role="banner">
                                    <div class="thumb overlay ">
                                        <img src="images/works/construction-feature-2.png" alt="Responsive & Mobile">
                                    </div>
                                    <p class="uppercase"><strong>Moteros</strong></p>
                                </header>
                                <p class="withpadding">Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis
                                    bibendum auctor, nisi elit consequat.</p>
                            </div>
                            <div class=" ok-md-3 ok-xsd-12 ok-sd-6 services text-center image">
                                <header role="banner">
                                    <div class="thumb overlay">
                                        <img src="images/works/construction-feature-3.png" alt="Responsive & Mobile">
                                    </div>
                                    <p class="uppercase"><strong>Moteros</strong></p>
                                </header>
                                <p class="withpadding">Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis
                                    bibendum auctor, nisi elit consequat.</p>
                            </div>
                            <div class=" ok-md-3 ok-xsd-12 ok-sd-6 services text-center image">
                                <header role="banner">
                                    <div class="thumb overlay">
                                        <img src="images/works/construction-feature-4.png" alt="Responsive & Mobile">
                                    </div>
                                    <p class="uppercase"><strong>Moteros</strong></p>
                                </header>
                                <p class="withpadding">Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis
                                    bibendum auctor, nisi elit consequat.</p>
                            </div>
                            
                        </div>
                    <div class="double-clear"></div>
                        <div class="ok-row">
                            <div class=" ok-md-3 ok-xsd-12 ok-sd-6 services  text-center image">
                                <header role="banner">
                                    <div class="thumb overlay">
                                        <img src="images/works/construction-feature-1.png" alt="Responsive & Mobile">
                                    </div>
                                    <p class="uppercase"><strong>Moteros</strong></p>
                                </header>
                                <p class="withpadding">Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis
                                    bibendum auctor, nisi elit consequat.</p>
                            </div>
                            <div class=" ok-md-3 ok-xsd-12 ok-sd-6 services text-center image">
                                <header role="banner">
                                    <div class="thumb overlay ">
                                        <img src="images/works/construction-feature-2.png" alt="Responsive & Mobile">
                                    </div>
                                    <p class="uppercase"><strong>Moteros</strong></p>
                                </header>
                                <p class="withpadding">Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis
                                    bibendum auctor, nisi elit consequat.</p>
                            </div>
                            <div class=" ok-md-3 ok-xsd-12 ok-sd-6 services text-center image">
                                <header role="banner">
                                    <div class="thumb overlay">
                                        <img src="images/works/construction-feature-3.png" alt="Responsive & Mobile">
                                    </div>
                                    <p class="uppercase"><strong>Moteros</strong></p>
                                </header>
                                <p class="withpadding">Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis
                                    bibendum auctor, nisi elit consequat.</p>
                            </div>
                            <div class=" ok-md-3 ok-xsd-12 ok-sd-6 services text-center image">
                                <header role="banner">
                                    <div class="thumb overlay">
                                        <img src="images/works/construction-feature-4.png" alt="Responsive & Mobile">
                                    </div>
                                    <p class="uppercase"><strong>Moteros</strong></p>
                                </header>
                                <p class="withpadding">Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis
                                    bibendum auctor, nisi elit consequat.</p>
                            </div>
                            
                        </div>
                    </div>
                <div class="double-clear"></div>
                <div class="double-clear"></div>
            
        </div>
        <!--! ALL CONTENTS -->

        <?php include 'inc/footer.php'; ?>

        <?php include 'inc/script.php'; ?>

    </div>
    <!--! LOADING -->

</body>

</html>
