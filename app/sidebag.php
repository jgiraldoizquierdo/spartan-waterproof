<?php $page = isset($_GET['menu'])?$_GET['menu']:'productos'; ?>

<?php include 'inc/configuracion.php'; ?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->

<?php include 'inc/head.php'; ?>

<body class="responsive">

    <!-- LOADING -->
    <div class="all_content loading">

        <?php include 'inc/header.php'; ?>
        
        <?php include 'inc/title.php'; ?>

        <!-- ALL CONTENTS -->
        <div class="dima-main">
          <section class="section section-colored" data-bg="#fafafa" style="background-color: rgb(250, 250, 250);">
            <div class="page-section-content overflow-hidden">
                    <div class="container">
                        <!-- WORKS -->
                        <div class="dima-container float-start">
                            <div class="post">
                                <div class="post-img">
                                    <div class="flexslider slider-thumb small-control" data-animation="slide" data-loop="true" data-nav="thumbnails">
                                        <ul class="slides">
                                            <!-- WORK (1) -->
                                            <li class="slide-item" data-thumb="images/productos/rollbag/small/Amarillo/AM-AM.jpg">
                                                <div class="flex-img">
                                                    <img src="images/productos/rollbag/big/Amarillo/AM-AM.jpg">
                                                </div>
                                            </li>
                                            <!-- WORK (1) -->
                                            <!-- WORK (2) -->
                                            <li class="slide-item" data-thumb="http://fpoimg.com/108x108">
                                                <div class="flex-img">
                                                    <img src="http://fpoimg.com/751x617">
                                                </div>
                                            </li>
                                            <!-- WORK (2) -->
                                            <!-- WORK (3) -->
                                            <li class="slide-item" data-thumb="http://fpoimg.com/108x108">
                                                <div class="flex-img">
                                                    <img src="http://fpoimg.com/751x617">
                                                </div>
                                            </li>
                                            <!-- WORK (3) -->
                                            <!-- WORK (4) -->
                                            <li class="slide-item" data-thumb="http://fpoimg.com/108x108">
                                                <div class="flex-img">
                                                    <img src="http://fpoimg.com/751x617">
                                                </div>
                                            </li>
                                            <!-- WORK (4) -->
                                            <!-- WORK (5) -->
                                            <li class="slide-item" data-thumb="http://fpoimg.com/108x108">
                                                <div class="flex-img">
                                                    <img src="images/works/5-v2.jpg" alt="">
                                                </div>
                                            </li>
                                            <!-- WORK (5) -->
                                            <!-- WORK (6) -->
                                            <li class="slide-item" data-thumb="images/works/s-6.jpg">
                                                <div class="flex-img">
                                                    <img src="http://fpoimg.com/751x617">
                                                </div>
                                            </li>
                                            <!-- WORK (6) -->
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--! WORKS -->
                        <aside role="complementary" class="dima-sidebar float-end hidden-xsd">
                            <!-- PROJECT INFO -->
                            <div class="box box-with-marge">
                                <h5 class="uppercase">SIDEBAG</h5>
                                <a class="float-end"><i class="logosesenta"></i></a>
                                <div class="clear"></div>
                                <div class="box-content">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore, ipsa reprehenderit
                                        fugit blanditiis voluptatum distinctio, ducimus nulla omnis ullam ipsam eaque vero
                                        quis nam hic nobis architecto nihil voluptatem molestiae cupiditate aspernatur at
                                        a, officiis! Quas nisi aut harum beatae fugit perspiciatis. Labore, ipsa reprehenderit
                                        fugit blanditiis voluptatum distinctio, ducimus nulla omnis ullam ipsam eaque vero
                                        quis nam hic nobis architecto nihil voluptatem.</p>
                                </div>
                            </div>
                            <!--! PROJECT INFO -->
                            <!-- PROJECT DETAILS -->
                            <div class="box box-with-marge">
                                <h4 class="uppercase box-titel">ficha técnica</h4>
                                <div class="box-content">
                                    <ul class="with-border">
                                        <li>
                                            <strong class="uppercase">Capacidad</strong>: 60lts</li>
                                        <li>
                                            <strong class="uppercase">Peso</strong>: 1kl</li>
                                        <li>
                                            <strong class="uppercase">Impermeable</strong>: si</li>
                                    </ul>
                                </div>
                            </div>
                            <!--! PROJECT DETAILS -->
                        </aside>
                    </div>
                </div>        
          </section>
        </div>
        <!--! ALL CONTENTS -->

        <?php include 'inc/footer.php'; ?>

        <?php include 'inc/script.php'; ?>

    </div>
    <!--! LOADING -->

</body>

</html>
